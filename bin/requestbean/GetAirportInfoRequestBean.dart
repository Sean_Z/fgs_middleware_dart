class GetAirportInfoRequestBean {
  GetAirportInfoRequestBean({
    required int serverType,
    required String airportIcao,
  }) {
    _serverType = serverType;
    _airportIcao = airportIcao;
  }

  GetAirportInfoRequestBean.fromJson(dynamic json) {
    _serverType = json['serverType'];
    _airportIcao = json['airportIcao'];
  }
  late int _serverType;
  late String _airportIcao;

  int get serverType => _serverType;
  String get airportIcao => _airportIcao;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['serverType'] = _serverType;
    map['airportIcao'] = _airportIcao;
    return map;
  }
}
