class GetIFUserByUserNameBean {
  GetIFUserByUserNameBean({
    required String username,
  }) {
    _username = username;
  }

  GetIFUserByUserNameBean.fromJson(dynamic json) {
    _username = json['username'];
  }
  late String _username;

  String get username => _username;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['username'] = _username;
    return map;
  }
}
