class OnlinePlanRequestBean {
  OnlinePlanRequestBean({
    required String flightId,
  }) {
    _flightId = flightId;
  }

  OnlinePlanRequestBean.fromJson(dynamic json) {
    _flightId = json['flightId'];
  }
  late String _flightId;

  String get flightId => _flightId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['flightId'] = _flightId;
    return map;
  }
}
