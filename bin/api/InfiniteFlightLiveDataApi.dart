import 'dart:convert';

import 'package:intl/intl.dart';

import '../bean/response/AirwayLineListBean.dart';
import '../bean/response/FgsAllFlightsBean.dart';
import '../bean/response/FgsUserInfoList.dart';
import '../bean/response/FlightCollectionRes.dart';
import '../bean/response/InfiniteFlightAirportStateBean.dart';
import '../bean/response/InfiniteFlightAirwayLineBean.dart';
import '../bean/response/InfiniteFlightAtcListBean.dart';
import '../bean/response/InfiniteFlightGetAllFlightsBean.dart';
import '../bean/response/InfiniteFlightOnlinePlane.dart';
import '../bean/response/InfiniteFlightUserGradeBean.dart';
import '../bean/response/InfiniteFlightUserInfoBean.dart';
import '../bean/response/OnlineFlightPlanBean.dart';
import '../core/AlgorithmCore.dart';
import '../request/InfiniteFlightLiveDataCaller.dart';
import 'FgsServiceApi.dart';

class InfiniteFlightLiveDataApi {
  /// 获取目标机场的ATIS信息
  static Future<String> getAtis(String airportICAO, int serverType, {bool shouldStopLoading = true, bool shouldShowError = true}) async {
    final _response = await InfiniteFlightLiveDataCaller.get('airport', '$airportICAO/', 'atis/', serverType: serverType);
    if (_response == null) {
      return 'ATC is Offline, No ATIS information!';
    }
    final _res = IFAtisBean.fromJson(_response);
    if (_res.errorCode == 0) {
      return _res.result ?? '';
    } else {
      return 'ATC is Offline, No ATIS information!';
    }
  }

  /// 获取ATC信息
  static Future<List<InfiniteFlightAtcBeanResult>> getATC(int serverType) async {
    final response = InfiniteFlightAtcListBean.fromJson(await InfiniteFlightLiveDataCaller.get('atc', '', '', serverType: serverType));
    return response.result;
  }

  /// 获取IF用户信息
  static Future<InfiniteFlightUserInfoBean> getInfiniteFlightUserInfo(String userId) async {
    final _requestData = {
      'userIds': [userId]
    };
    return InfiniteFlightUserInfoBean.fromJson(await InfiniteFlightLiveDataCaller.postLiveData(_requestData));
  }

  /// 获取IF用户等级信息
  static Future<InfiniteFlightUserGradeBean> getInfiniteFlightUserGrade(String userId) async {
    return InfiniteFlightUserGradeBean.fromJson(await InfiniteFlightLiveDataCaller.get('user', 'grade/', userId, noSession: true));
  }

  /// 获取在线飞行计划
  static Future<OnlineFlightPlanBean> getOnlinePlan(String flightId) async {
    try {
      final res = await InfiniteFlightLiveDataCaller.get('flight', '$flightId/', 'flightplan', noSession: true);
      final response = InfiniteFlightOnlinePlane.fromJson(res);
      if (response.errorCode == 0) {
        final route = response.result!.waypoints;
        final routerInfo = route.join(' ');
        final departure = route[0] == 'WPT' ? route[1] : route[0];
        final arrival = route[route.length - 1];
        final flightPlanItems = response.result!.flightPlanItems;
        final departurePosition = flightPlanItems![0].location!.toJson();
        final arrivalPosition = flightPlanItems.last.location!.toJson();
        final wholeRangePosition = [];
        for (var _item in flightPlanItems) {
          if (_item.location!.latitude != 0 && _item.location!.longitude != 0) {
            final latitude = double.parse(_item.location!.latitude.toString());
            final longitude = double.parse(_item.location!.longitude.toString());
            final point = {'latitude': latitude, 'longitude': longitude};
            wholeRangePosition.add(point);
          }
        }
        final result = OnlineFlightPlanBean.fromJson({
          'routerInfo': routerInfo,
          'departure': departure,
          'arrival': arrival,
          'departurePosition': departurePosition,
          'arrivalPosition': arrivalPosition,
          'wholeRangePosition': wholeRangePosition
        });
        return result;
      } else {
        throw Exception('get plan exception');
      }
    } catch (e) {
      final _result = OnlineFlightPlanBean(
        routeInfo: '',
        departure: '',
        arrival: '',
        departurePosition: DeparturePosition(latitude: 0, longitude: 0, altitude: 0),
        wholeRangePosition: [],
        arrivalPosition: ArrivalPosition(latitude: 0, longitude: 0, altitude: 0),
      );
      return _result;
    }
  }

  /// 获取在线航线
  static Future<List<AirwayLineListBean>> getAirwayLine(String flightId) async {
    try {
      final _res = await InfiniteFlightLiveDataCaller.get('flight', '$flightId/', 'route', noSession: true);
      final _response = InfiniteFlightAirwayLineBean.fromJson(_res);
      if (_response.errorCode == 0) {
        final _airwayList = <AirwayLineListBean>[];
        for (var _item in _response.result!) {
          _airwayList.add(AirwayLineListBean(
            latitude: _item.latitude!,
            longitude: _item.longitude!,
            time: _item.date!,
            altitude: _item.altitude!,
            groundSpeed: _item.groundSpeed!,
          ));
        }
        return _airwayList;
      } else {
        throw Exception('get airway plan failed!');
      }
    } catch (e) {
      throw Exception('get airway plan failed!');
    }
  }

  /// 获取用户飞行状态包装准备提交
  static Future getLogInfo(FgsOnlineFlightData flightBean) async {
    try {
      final resList = await Future.wait([
        InfiniteFlightLiveDataApi.getOnlinePlan(flightBean.flightId),
        InfiniteFlightLiveDataApi.getAirwayLine(flightBean.flightId),
      ]);
      final fetchOnlinePlanResponse = resList[0];
      final airwayLineResponse = resList[1];
      final airwayLineResponseRes = airwayLineResponse as List<AirwayLineListBean>;
      final boardingTime = DateTime.parse(airwayLineResponseRes[0].time);
      final departureTime = DateTime.parse(FlightJudgment.getDepartureTime(airwayLineResponseRes));
      final range = CalculateMethod.entireDistance(airwayLineResponseRes);
      final currentTime = DateTime.now();
      final flightTime = currentTime.difference(departureTime);
      final baseInfo = fetchOnlinePlanResponse as OnlineFlightPlanBean;
      final liveryName = DataHandler.getLiveryName(flightBean.livery);
      final aircraftName = flightBean.livery[0].aircraftName;
      final speed = flightBean.speed.floor().abs();
      if (speed != 0) {
        throw Exception('Please log flight until set break');
      }
      if (baseInfo.routeInfo.isEmpty) {
        throw Exception('Flight plan is missed, please change a point in your flight plan or set plan again');
      }
      return LogInfoClass(
          departure: baseInfo.departure,
          arrival: baseInfo.arrival,
          departureTime: DateFormat('MMM d, H:mm', 'en_US').format(departureTime.toLocal()),
          arrivalTime: DateFormat('MMM d, H:mm', 'en_US').format(DateTime.now()),
          flightNumber: flightBean.callSign,
          route: baseInfo.routeInfo,
          flightId: flightBean.flightId,
          livery: liveryName,
          aircraft: aircraftName,
          flightTime: flightTime.inMinutes,
          distance: range,
          boardingTime: DateFormat('MMM d, H:mm', 'en_US').format(boardingTime.toLocal()),
          airwayLine: airwayLineResponseRes);
    } catch (e) {
      return e;
    }
  }

  static Future<Map<String, double>> getRangeInfo(FgsOnlineFlightData flightBean) async {
    final _currentSpeed = flightBean.speed;
    final _response = await Future.wait([
      InfiniteFlightLiveDataApi.getOnlinePlan(flightBean.flightId),
      InfiniteFlightLiveDataApi.getAirwayLine(flightBean.flightId),
    ]);
    final onlinePlaneResponse = _response[0] as OnlineFlightPlanBean;
    final _airwayResponse = _response[1] as List<AirwayLineListBean>;
    final wholeRangePosition = onlinePlaneResponse.wholeRangePosition;
    final wholeRange = CalculateMethod.entireWholeDistance(wholeRangePosition);
    final alreadyFlewRange = CalculateMethod.entireDistance(_airwayResponse);
    final _remainTime = (wholeRange - alreadyFlewRange) / _currentSpeed;
    return {'wholeRange': wholeRange, 'flewRange': alreadyFlewRange, 'remainFlightTime': _remainTime};
  }

  static Future<List<FgsOnlineFlightData>> getServerFlight(int serverType, {String specificUserName = ''}) async {
    final _jsonString = json.decode(await FgsServiceApi.getAircraftJson()) as List;
    final _liverList = <Livery>[];
    for (var i = 0; i < _jsonString.length; ++i) {
      final liverListMap = Livery.fromJson(_jsonString[i]);
      _liverList.add(liverListMap);
    }
    try {
      final _serverFlightResponse =
          InfiniteFlightGetAllFlightsBean.fromJson(await InfiniteFlightLiveDataCaller.get('flights', '', '', serverType: serverType));

      final onlineFlightDataList = <InfiniteFlightAllFlightsResult>[];
      if (_serverFlightResponse.errorCode == 0) {
        for (var _item in _serverFlightResponse.result) {
          onlineFlightDataList.add(_item);
        }
      }
      final _filteredList = specificUserName.isNotEmpty
          ? onlineFlightDataList.where((element) => element.username == specificUserName).toList()
          : onlineFlightDataList;
      final _listData = <FgsOnlineFlightData>[];
      for (var _item in _filteredList) {
        final _targetLivery = _liverList.where((element) => element.liveryId == _item.liveryId).toList();
        if (_targetLivery.isNotEmpty) {
          final _targetLiveryItem = _targetLivery[0];
          final fgsItem = FgsOnlineFlightData(
              position: Position(latitude: _item.latitude, longitude: _item.longitude),
              angle: double.parse(_item.heading.toString()),
              liveryId: _item.liveryId,
              flightId: _item.flightId,
              aircraftId: _item.aircraftId,
              altitude: _item.altitude,
              callSign: _item.callsign,
              displayName: _item.username,
              userId: _item.userId,
              speed: _item.speed,
              verticalSpeed: _item.verticalSpeed,
              livery: [
                Livery(
                    aircraftId: _item.aircraftId,
                    liveryId: _item.liveryId,
                    liveryName: _targetLiveryItem.liveryName ?? 'unknown',
                    aircraftName: _targetLiveryItem.aircraftName)
              ]);
          _listData.add(fgsItem);
        }
      }
      return _listData;
    } catch (e) {
      throw Exception('Getting all flight data exception');
    }
  }

  /// 根据用户名获取IF用户信息
  static Future<InfiniteFlightUserInfoBean> getInfiniteFlightUserInfoByUserName(String userName) async {
    final _requestData = {
      'discourseNames': [userName]
    };
    final data = await InfiniteFlightLiveDataCaller.postLiveData(_requestData);
    return InfiniteFlightUserInfoBean.fromJson(data);
  }

  /// 获取所有FGS成员飞行信息
  static Future<List<List<FgsOnlineFlightData>>> getFgsMemberFlight() async {
    final _resList = await Future.wait([
      InfiniteFlightLiveDataApi.getAllServerFlight(),
      FgsServiceApi.getFgsUserInfoList(),
    ]);
    final _infiniteFlightResult = _resList.first as List<FgsOnlineFlightData>;
    final _fgsMemberList = _resList[1] as FgsUserInfoList;
    final _fgsUserNameList = [];
    for (var element in _fgsMemberList.list) {
      _fgsUserNameList.add(element.username);
    }
    final result = _infiniteFlightResult.where((element) => _fgsUserNameList.contains(element.displayName)).toList();
    final daData = await _getDA(result, result);
    return daData;
  }

  /// 获取机场信息
  static Future<AirportStateResponseBean> getAirportState(String airportCode, int serverType) async {
    final _responseList = await Future.wait([
      InfiniteFlightLiveDataCaller.get('airport/', airportCode, '/status/', serverType: serverType),
      InfiniteFlightLiveDataApi.getServerFlight(serverType),
    ]);
    final _airportResponse = InfiniteFlightAirportStateBean.fromJson(_responseList[0]);
    final _allFlightResponse = _responseList[1] as List<FgsOnlineFlightData>;
    final _inboundList = <FgsOnlineFlightData>[];
    final _outboundList = <FgsOnlineFlightData>[];
    final _atcFacilitiesList = <AtcFacilities>[];
    if (_airportResponse.errorCode == 0) {
      for (var element in (_airportResponse.result!.atcFacilities!)) {
        _atcFacilitiesList.add(element);
      }
      final _inboundFlightsCount = _airportResponse.result!.inboundFlightsCount;
      final _outboundFlightsCount = _airportResponse.result!.outboundFlightsCount;
      for (var _item in _allFlightResponse) {
        for (var _airportItem in _airportResponse.result!.inboundFlights!) {
          if (_airportItem == _item.flightId) {
            _inboundList.add(_item);
          }
        }
      }
      for (var _item in _allFlightResponse) {
        for (var _airportItem in _airportResponse.result!.outboundFlights!) {
          if (_airportItem == _item.flightId) {
            _outboundList.add(_item);
          }
        }
      }
      final _daResult = await _getDA(_inboundList, _outboundList);
      final _result = AirportStateResponseBean(
        outboundFlightsCount: _outboundFlightsCount ?? 0,
        inboundFlightsCount: _inboundFlightsCount ?? 0,
        atcFacilitiesList: _atcFacilitiesList,
        inboundList: _daResult.first,
        outboundList: _daResult[1],
      );
      return _result;
    } else {
      throw Exception('get airport info exception');
    }
  }

  static List<KvClass> countAircraft(List<FgsOnlineFlightData> list) {
    final finalList = [KvClass()];
    final map = {};
    for (var i = 0; i < list.length; i++) {
      final element = list[i];
      final aircraftName = element.livery[0].aircraftName;
      if (map.keys.toList().contains(aircraftName)) {
        map[aircraftName] = map[aircraftName] + 1;
      } else {
        map[aircraftName] = 1;
      }
    }
    map.forEach((key, value) {
      finalList.add(KvClass(name: key.toString(), value: int.parse(value.toString())));
    });
    return finalList;
  }

  /// 获取全服飞机
  static Future<TargetFlightLocateClass?> getTargetInAllServerFlight(String specificUsername) async {
    final _responseList = await Future.wait<List<FgsOnlineFlightData>>([
      InfiniteFlightLiveDataApi.getServerFlight(0),
      InfiniteFlightLiveDataApi.getServerFlight(1),
      InfiniteFlightLiveDataApi.getServerFlight(2),
    ]);
    for (var i = 0; i < _responseList.length; i++) {
      final _element = _responseList[i];
      if (_element.where((element) => element.displayName == specificUsername).toList().isNotEmpty) {
        final targetFlight = _element.where((element) => element.displayName == specificUsername).toList()[0];
        return TargetFlightLocateClass(targetFlight: targetFlight, serverType: i);
      }
    }
  }

  static Future<List<FgsOnlineFlightData>> getAllServerFlight() async {
    final _responseList = await Future.wait<List<FgsOnlineFlightData>>([
      InfiniteFlightLiveDataApi.getServerFlight(0),
      InfiniteFlightLiveDataApi.getServerFlight(1),
      InfiniteFlightLiveDataApi.getServerFlight(2),
    ]);
    return [..._responseList[0], ..._responseList[1], ..._responseList[2]];
  }

  static Future<FlightCollectionRes> getFlightCollection() async {
    final _allFlight = await getAllServerFlight();
    final res = countAircraft(_allFlight);
    res.sort((KvClass left, KvClass right) => (right.value ?? 0).compareTo(left.value ?? 0));
    final result = FlightCollectionRes(kvClass: res, totalAircraft: res.length);
    return result;
  }

  static Future<List<List<FgsOnlineFlightData>>> _getDA(
      List<FgsOnlineFlightData> inboundFlight, List<FgsOnlineFlightData> outboundFlight) async {
    final inboundIcaoRequestList = <Future<OnlineFlightPlanBean>>[];
    final outboundIcaoRequestList = <Future<OnlineFlightPlanBean>>[];
    for (var element in inboundFlight) {
      inboundIcaoRequestList.add(InfiniteFlightLiveDataApi.getOnlinePlan(element.flightId));
    }
    for (var element in outboundFlight) {
      outboundIcaoRequestList.add(InfiniteFlightLiveDataApi.getOnlinePlan(element.flightId));
    }
    final response = await Future.wait([Future.wait(inboundIcaoRequestList), Future.wait(outboundIcaoRequestList)]);
    final originInboundFlight = inboundFlight;
    final originOutboundFlight = outboundFlight;

    for (var i = 0; i < originInboundFlight.length; i++) {
      var element = originInboundFlight[i];
      element.departure = response[0][i].departure;
      element.arrival = response[0][i].arrival;
    }

    for (var i = 0; i < originOutboundFlight.length; i++) {
      var element = originOutboundFlight[i];
      element.departure = response[1][i].departure;
      element.arrival = response[1][i].arrival;
    }
    return [originInboundFlight, originOutboundFlight];
  }
}

class IFAtisBean {
  int? errorCode;
  String? result;

  IFAtisBean({required this.errorCode, required this.result});

  IFAtisBean.fromJson(dynamic json) {
    errorCode = json['errorCode'] as int;
    result = json['result'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['errCode'] = errorCode;
    map['result'] = result;
    return map;
  }
}

class AirportStateResponseBean {
  final int inboundFlightsCount;
  final int outboundFlightsCount;
  final List<AtcFacilities> atcFacilitiesList;
  final List<FgsOnlineFlightData> inboundList;
  final List<FgsOnlineFlightData> outboundList;

  AirportStateResponseBean(
      {required this.outboundFlightsCount,
      required this.inboundFlightsCount,
      required this.atcFacilitiesList,
      required this.inboundList,
      required this.outboundList});

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['inboundFlightsCount'] = inboundFlightsCount;
    map['outboundFlightsCount'] = outboundFlightsCount;
    map['atcFacilitiesList'] = atcFacilitiesList;
    map['inboundList'] = inboundList;
    map['outboundList'] = outboundList;
    return map;
  }
}

class TargetFlightLocateClass {
  final FgsOnlineFlightData targetFlight;
  final int serverType;

  TargetFlightLocateClass({required this.targetFlight, required this.serverType});
}

class LogInfoClass {
  final String departure;
  final String arrival;
  final String departureTime;
  final String arrivalTime;
  final String flightNumber;
  final String route;
  final String flightId;
  final String livery;
  final String aircraft;
  final int flightTime;
  final double distance;
  final String boardingTime;
  final List<AirwayLineListBean> airwayLine;

  LogInfoClass(
      {required this.departure,
      required this.arrival,
      required this.departureTime,
      required this.arrivalTime,
      required this.flightNumber,
      required this.route,
      required this.flightId,
      required this.livery,
      required this.aircraft,
      required this.flightTime,
      required this.distance,
      required this.boardingTime,
      required this.airwayLine});
}
