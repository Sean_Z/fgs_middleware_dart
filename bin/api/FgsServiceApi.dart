import 'dart:io';

import '../bean/response/CommonResponse.dart';
import '../bean/response/FgsAircraftAdviceBean.dart';
import '../bean/response/FgsFlightPlanBean.dart';
import '../bean/response/FgsGetFileListBean.dart';
import '../bean/response/FgsUserInfoBean.dart';
import '../bean/response/FgsUserInfoList.dart';
import '../bean/response/FlightLogsListBean.dart';
import '../bean/response/LoginResponseBean.dart';
import '../constant/Constant.dart';
import '../core/log.dart';
import '../request/FgsCaller.dart';
import 'InfiniteFlightLiveDataApi.dart';

class FgsServiceApi {
  /// login by account and password
  static Future<LoginResponseBean> login(String username, String password) async {
    return LoginResponseBean.fromJson(await FgsCaller.post('user/login', {'username': username, 'password': password, 'type': 0}));
  }

  /// fingerprint login
  static Future<LoginResponseBean> fingerprintLogin(String username) async {
    return LoginResponseBean.fromJson(await FgsCaller.post('user/fingerprintLogin', {'username': username}));
  }

  /// register
  static Future<CommonResponse> register(Map<String, Object> requestData) async {
    return CommonResponse.fromJson(await FgsCaller.post('user/application', requestData));
  }

  /// getFlightLogs by pageNo and pageSize
  static Future<FlightLogListBean> getFlightLog(int pageNo, int pageSize) async {
    return FlightLogListBean.fromJson(await FgsCaller.post('fms/flightLog', {'pageNo': pageNo, 'pageSize': pageSize}));
  }

  /// log flight into FGS Flight log database
  static Future<CommonResponse> logFlight(LogInfoClass requestData) async {
    final airway = <Map>[];
    for (var element in requestData.airwayLine) {
      airway.add({
        'altitude': element.altitude,
        'groundSpeed': element.groundSpeed,
        'longitude': element.longitude,
        'latitude': element.latitude,
        'time': element.time
      });
    }
    final requestBody = {
      'departure': requestData.departure,
      'arrival': requestData.arrival,
      'departureTime': requestData.departureTime,
      'arrivalTime': requestData.arrivalTime,
      'flightNumber': requestData.flightNumber,
      'route': requestData.route,
      'flightId': requestData.flightId,
      'livery': requestData.livery,
      'aircraft': requestData.aircraft,
      'flightTime': requestData.flightTime,
      'distance': requestData.distance,
      'boardingTime': requestData.boardingTime,
      'airwayLine': airway
    };
    return CommonResponse.fromJson(await FgsCaller.post('fms/logFlight', requestBody));
  }

  /// get FGS user info include base info and history flight info
  static Future<FgsUserInfoBean> getFgsUserInfo() async {
    return FgsUserInfoBean.fromJson(await FgsCaller.post('user/getUserInfo', {}));
  }

  static Future<FgsUserInfoList> getFgsUserInfoList() async {
    final userInfoList = FgsUserInfoList(
      endRow: 1,
      hasNextPage: false,
      hasPreviousPage: false,
      isFirstPage: true,
      isLastPage: true,
      list: [],
      navigateFirstPage: 1,
      navigateLastPage: 1,
      navigatePages: 1,
      navigatepageNums: [],
      nextPage: 1,
      pageNum: 1,
      pageSize: 20,
      pages: 1,
      prePage: 1,
      size: 10,
      startRow: 1,
      total: 20,
    );
    // final real = FgsUserInfoList.fromJson(await FgsCaller.post('user/getUserInfo', {'isUserList': 0}));
    return userInfoList;
  }

  /// subscribe flight send sms or email when flight time remain less then 30 minutes
  static Future<CommonResponse> subscribeFlight(String displayName, int sessionId) async {
    final requestData = {'username': displayName, 'sessionId': sessionId};
    return CommonResponse.fromJson(await FgsCaller.post('fms/subscribeFlight', requestData));
  }

  /// generate flight plan
  static Future<FgsFlightPlanBean> getFlightPlan(String departureICAO, String arrivalICAO) async {
    final requestBody = <String, String>{'fromICAO': departureICAO, 'toICAO': arrivalICAO};
    return FgsFlightPlanBean.fromJson(await FgsCaller.post('common/getPlan', requestBody));
  }

  static Future<bool> getSpecificAirportChart(String query, [bool shouldShowTip = true]) async {
    final _response =
        FgsGetFileListBean.fromJson(await FgsCaller.get('fms/getAirportChartList', 'pageSize=20&pageNo=0&specificFileName=$query.pdf'));
    if (_response.list.isEmpty) {
      if (shouldShowTip) {
        throw ('target not found in fgs charts system!');
      }
      return false;
    } else {
      return true;
    }
  }

  /// 从腾讯云存储通获取机场航图
  static String getAirportChartsFromCos(String chartsType, String region, String chartName) {
    switch (chartsType) {
      case '1':
        return '${Links.FGS_STORAGE_BASE_URL}charts/AirportCharts/$region/$chartName';
      case '2':
        return '${Links.FGS_STORAGE_BASE_URL}charts/FctmCharts/$chartName';
      default:
        return '';
    }
  }

  /// 获取机型建议库
  static Future<List<FgsAircraftAdviceBean>> getAircraftAdvice() async {
    final _list = <FgsAircraftAdviceBean>[];
    for (var element in (await FgsCaller.get('fms/getAllAircraftAdvice', '') as List)) {
      _list.add(FgsAircraftAdviceBean.fromJson(element));
    }
    return _list;
  }

  static void logError(
    String error,
  ) {
    final _errorPackage = {
      'errorMsg': error,
      'errorTime': DateTime.now().toString(),
      'errorPlatform': Platform.environment.toString(),
    };
    Log.logger.e(_errorPackage);
    FgsCaller.post('common/log', _errorPackage);
  }

  static Future<String> getAircraftJson() async {
    final rootPath = Directory.current.absolute.path;
    final file = File('$rootPath/static/aircraftInfo.json');
    return file.readAsStringSync();
  }
}
