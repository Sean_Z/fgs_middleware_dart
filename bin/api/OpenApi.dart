import 'package:dio/dio.dart';

import '../bean/response/FplIdRes.dart';
import '../bean/response/FplInfoRes.dart';
import '../constant/Constant.dart';

class OpenApi {
  final dio = Dio();

  get longitude => null;
  get latitude => null;
  Future<FplRes> getFpl(String fromICAO, String toICAO) async {
    const config = {
      'auth': {'username': Links.FLP_DB_KEY, 'password': ''}
    };
    final fplIdResponse =
        await dio.post<FplIdRes>(Links.FLP_DB_URL, data: {'fromICAO': fromICAO, 'toICAO': toICAO}, options: Options(headers: config));
    final responseData = fplIdResponse.data!;
    final planId = responseData.id;
    final distance = responseData.distance;
    final waypointResponse = await dio.get<FplInfoRes>(Links.FLP_DB_PLAN_URL + planId.toString());
    final fplInfoRes = waypointResponse.data!;
    final waypoints = [];
    final coordinateList = [];
    for (var i = 0; i < fplInfoRes.route.nodes.length; i++) {
      waypoints.add(fplInfoRes.route.nodes[i].ident);
      coordinateList.add({latitude: fplInfoRes.route.nodes[i].lat, longitude: fplInfoRes.route.nodes[i].lon});
    }
    final departureName = fplInfoRes.fromName;
    final arrivalName = fplInfoRes.toName;
    return FplRes(waypoints.join(' '), distance, coordinateList, departureName, arrivalName, 0, 'success');
  }
}

class FplRes {
  final String routeInfo;
  final double distance;
  final List coordinateList;
  final String departureName;
  final String arrivalName;
  final int status;
  final String message;
  FplRes(this.routeInfo, this.distance, this.coordinateList, this.departureName, this.arrivalName, this.status, this.message);
}
