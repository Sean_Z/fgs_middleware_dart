enum ServerType { prod, local, dev, wifi }

class AppVersionInfo {
  static const appVersion = '1.1.5';
}

class IFServerMap {
  static final serverMap = {
    0: 'Expert Server',
    1: 'Training Server',
    2: 'Casual Server',
  };
}

class FgsServer {
  static const baseUrlMap = {
    0: 'https://flightglobalstu.xyz/FGS_MiddleWare/',
    1: 'http://127.0.0.1:529/FGS_MiddleWare/',
    2: 'http://127.0.0.1:5700/',
    3: 'http://192.168.1.100:529/FGS_MiddleWare/',
  };
  static final baseUrl = baseUrlMap[ServerType.prod.index]!;
}

class KeyManager {
  static const amapKey = '5870e6f10b84f84346040fb6de4fcb38';
  static const amapGeoKey = 'ff46edce02ee0bc7dfa81f11f5ca80a2';
}

class ConfigList {
  static const encryptList = AppVersionInfo.appVersion == '1.1.3'
      ? [
          '/infiniteFlight/fetchOnlinePlan',
          '/infiniteFlight/airwayLine',
          '/infiniteFlight/getAllServerFlight',
          '/user/fingerprintLogin',
          '/user/login'
        ]
      : [
          '/infiniteFlight/fetchOnlinePlan',
          '/infiniteFlight/airwayLine',
          '/infiniteFlight/getAllServerFlight',
        ];
  static const tokenWhiteList = [
    '/user/resetPassword',
    '/user/application',
    '/user/sendVerifyCode',
    '/user/login',
    '/user/fingerprintLogin',
    '/common/getAppInfo',
    '/fms/uploadAirportCharts',
  ];
}

class OwnAircraftPhotoList {
  /// After StartActivityEventHandler start this variable will be rewrite
  static var ownAircraftPhotoList = <String>[];
}

class Links {
  static const AIRLINE_ICON_URL = 'https://www.flightradar24.com/static/images/data/operators/';
  static const String FGS_STORAGE_BASE_URL = 'https://fgs-1300812707.cos.ap-shanghai.myqcloud.com/';
  static const String AIRCRAFT_PHOTO_BASE_LINK = 'https://cdn.liveflightapp.com/aircraft-images/';
  static const String UNKNOWN_AIRCRAFT = 'https://cdn.liveflightapp.com/aircraft-images/default.jpg';
  static const String FPL_TO_IF_RANDOM_FPL = 'https://fpltoif.com/simbrief';
  static const String TESTFLIGHT_SCHEME_URL = 'itms-beta://';
  static const String WECHAT_SCHEME_URL = 'weixin://';
  static const String FLP_DB_KEY = 'ZN05FcLi82bBJOySk0FkTpuk6NsNiCFdEkqT8iQY';
  static const String FLP_DB_URL = 'https://api.flightplandatabase.com/auto/generate';
  static const String FLP_DB_PLAN_URL = 'https://api.flightplandatabase.com/plan/';
  static const String AndroidReleaseApkUrl = 'https://fgs-1300812707.cos.ap-shanghai.myqcloud.com/Apk/app-release.apk';
  static const String OSM_MAP_URL =
      'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoic2VhbjE5OTciLCJhIjoiY2tudmhudDBqMG1zbDJvcm1tM2VhcGJ2bSJ9.N3MiY_b4mfA98t_XLfFvGw';

  static String getOsmTheme(OSM_MAP_STYLE osmMapType) {
    switch (osmMapType) {
      case OSM_MAP_STYLE.streets:
        return 'mapbox/streets-v11';
      case OSM_MAP_STYLE.light:
        return 'mapbox/light-v10';
      case OSM_MAP_STYLE.dark:
        return 'mapbox/dark-v10';
      case OSM_MAP_STYLE.satellite:
        return 'mapbox/satellite-v9';
      default:
        return 'navigation-c-night-v4';
    }
  }

  static String getSessionId(int serverType) {
    switch (serverType) {
      // expertServerSession
      case 0:
        return '7e5dcd44-1fb5-49cc-bc2c-a9aab1f6a856';
      // trainingServerSession
      case 1:
        return '6a04ffe8-765a-4925-af26-d88029eeadba';
      // casualServerSession
      case 2:
        return 'd01006e4-3114-473c-8f69-020b89d02884';
      default:
        return '7e5dcd44-1fb5-49cc-bc2c-a9aab1f6a856';
    }
  }
}

enum OSM_MAP_STYLE {
  streets,
  light,
  dark,
  satellite,
}

enum InfiniteFlightServerType { expert, training, casual }

enum FileBizType { avatar, airport, fctm }
