/// routeInfo : "ZBAA LADIX GOTVA YQG DALIM ABTUB UDINO DPX OMUDI LAGAL ATVAD SUBKU NIXEM XUTGU PIMOL ZSSS"
/// distance : 610.1025912777301
/// coordinateList : [{"latitude":40.0787,"longitude":116.59500000000003},{"latitude":39.129999999999995,"longitude":116.99200000000002},{"latitude":37.35669999999999,"longitude":117.16500000000002},{"latitude":36.83330000000001,"longitude":117.21500000000003},{"latitude":36.4183,"longitude":117.21299999999997},{"latitude":36,"longitude":117.368},{"latitude":34.8233,"longitude":117.803},{"latitude":34.2783,"longitude":117.99799999999999},{"latitude":33.97059999999999,"longitude":118.267},{"latitude":33.4667,"longitude":118.70499999999998},{"latitude":33.2967,"longitude":118.85500000000002},{"latitude":33.19669999999999,"longitude":118.94},{"latitude":32.9417,"longitude":119.15999999999997},{"latitude":32.463300000000004,"longitude":119.57499999999999},{"latitude":32.246700000000004,"longitude":119.762},{"latitude":31.198099999999997,"longitude":121.334}]
/// departureName : "Beijing-Capital"
/// arrivalName : "SHANGHAI HONGQIAO INTL"
/// status : 0
/// message : "success"

class FgsFlightPlanBean {
  String? routeInfo;
  double? distance;
  List<CoordinateList>? coordinateList;
  String? departureName;
  String? arrivalName;
  late int status;
  late String message;

  FgsFlightPlanBean({this.routeInfo, this.distance, this.coordinateList, this.departureName, this.arrivalName, required this.status, required this.message});

  FgsFlightPlanBean.fromJson(dynamic json) {
    routeInfo = (json['routeInfo'] ?? '') as String;
    distance = double.parse((json['distance'] ?? 0.0).toString());
    if (json['coordinateList'] != null) {
      coordinateList = [];
      json['coordinateList'].forEach((v) {
        coordinateList?.add(CoordinateList.fromJson(v));
      });
    }
    departureName = (json['departureName'] ?? '') as String;
    arrivalName = (json['arrivalName'] ?? '') as String;
    status = json['status'] as int;
    message = json['message'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['routeInfo'] = routeInfo;
    map['distance'] = distance;
    if (coordinateList != null) {
      map['coordinateList'] = coordinateList?.map((v) => v.toJson()).toList();
    }
    map['departureName'] = departureName;
    map['arrivalName'] = arrivalName;
    map['status'] = status;
    map['message'] = message;
    return map;
  }
}

/// latitude : 40.0787
/// longitude : 116.59500000000003

class CoordinateList {
  double? latitude;
  double? longitude;

  CoordinateList({this.latitude, this.longitude});

  CoordinateList.fromJson(dynamic json) {
    latitude = double.parse(json['latitude'].toString());
    longitude = double.parse(json['longitude'].toString());
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    return map;
  }
}
