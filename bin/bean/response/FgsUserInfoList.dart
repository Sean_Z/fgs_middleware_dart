class FgsUserInfoList {
  FgsUserInfoList({
    required int endRow,
    required bool hasNextPage,
    required bool hasPreviousPage,
    required bool isFirstPage,
    required bool isLastPage,
    required List<UserList> list,
    required int navigateFirstPage,
    required int navigateLastPage,
    required int navigatePages,
    required List navigatepageNums,
    required int nextPage,
    required int pageNum,
    required int pageSize,
    required int pages,
    required int prePage,
    required int size,
    required int startRow,
    required int total,
  }) {
    _endRow = endRow;
    _hasNextPage = hasNextPage;
    _hasPreviousPage = hasPreviousPage;
    _isFirstPage = isFirstPage;
    _isLastPage = isLastPage;
    _list = list;
    _navigateFirstPage = navigateFirstPage;
    _navigateLastPage = navigateLastPage;
    _navigatePages = navigatePages;
    _navigatepageNums = navigatepageNums;
    _nextPage = nextPage;
    _pageNum = pageNum;
    _pageSize = pageSize;
    _pages = pages;
    _prePage = prePage;
    _size = size;
    _startRow = startRow;
    _total = total;
  }

  FgsUserInfoList.fromJson(dynamic json) {
    _endRow = json['endRow'] as int;
    _hasNextPage = json['hasNextPage'] as bool;
    _hasPreviousPage = json['hasPreviousPage'] as bool;
    _isFirstPage = json['isFirstPage'] as bool;
    _isLastPage = json['isLastPage'] as bool;
    if (json['list'] != null) {
      _list = [];
      json['list'].forEach((v) {
        _list.add(UserList.fromJson(v));
      });
    }
    _navigateFirstPage = json['navigateFirstPage'] as int;
    _navigateLastPage = json['navigateLastPage'] as int;
    _navigatePages = json['navigatePages'] as int;
    _navigatepageNums = json['navigatepageNums'] as List;
    _nextPage = json['nextPage'] as int;
    _pageNum = json['pageNum'] as int;
    _pageSize = json['pageSize'] as int;
    _pages = json['pages'] as int;
    _prePage = json['prePage'] as int;
    _size = json['size'] as int;
    _startRow = json['startRow'] as int;
    _total = json['total'] as int;
  }
  late int _endRow;
  late bool _hasNextPage;
  late bool _hasPreviousPage;
  late bool _isFirstPage;
  late bool _isLastPage;
  late List<UserList> _list;
  late int _navigateFirstPage;
  late int _navigateLastPage;
  late int _navigatePages;
  late List _navigatepageNums;
  late int _nextPage;
  late int _pageNum;
  late int _pageSize;
  late int _pages;
  late int _prePage;
  late int _size;
  late int _startRow;
  late int _total;

  int get endRow => _endRow;
  bool get hasNextPage => _hasNextPage;
  bool get hasPreviousPage => _hasPreviousPage;
  bool get isFirstPage => _isFirstPage;
  bool get isLastPage => _isLastPage;
  List<UserList> get list => _list;
  int get navigateFirstPage => _navigateFirstPage;
  int get navigateLastPage => _navigateLastPage;
  int get navigatePages => _navigatePages;
  List get navigatepageNums => _navigatepageNums;
  int get nextPage => _nextPage;
  int get pageNum => _pageNum;
  int get pageSize => _pageSize;
  int get pages => _pages;
  int get prePage => _prePage;
  int get size => _size;
  int get startRow => _startRow;
  int get total => _total;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['endRow'] = _endRow;
    map['hasNextPage'] = _hasNextPage;
    map['hasPreviousPage'] = _hasPreviousPage;
    map['isFirstPage'] = _isFirstPage;
    map['isLastPage'] = _isLastPage;
    map['list'] = _list.map((v) => v.toJson()).toList();
    map['navigateFirstPage'] = _navigateFirstPage;
    map['navigateLastPage'] = _navigateLastPage;
    map['navigatePages'] = _navigatePages;
    map['navigatepageNums'] = _navigatepageNums;
    map['nextPage'] = _nextPage;
    map['pageNum'] = _pageNum;
    map['pageSize'] = _pageSize;
    map['pages'] = _pages;
    map['prePage'] = _prePage;
    map['size'] = _size;
    map['startRow'] = _startRow;
    map['total'] = _total;
    return map;
  }
}

class UserList {
  UserList({
    required String age,
    required int bonus,
    required String country,
    required int createtime,
    required int iscommercial,
    required String mail,
    required String position,
    required int rank,
    required int rankbonus,
    required int role,
    required int updatetime,
    required int userid,
    required String username,
  }) {
    _age = age;
    _bonus = bonus;
    _country = country;
    _createtime = createtime;
    _iscommercial = iscommercial;
    _mail = mail;
    _position = position;
    _rank = rank;
    _rankbonus = rankbonus;
    _role = role;
    _updatetime = updatetime;
    _userid = userid;
    _username = username;
  }

  UserList.fromJson(dynamic json) {
    _age = json['age'] as String;
    _bonus = json['bonus'] as int;
    _country = json['country'] as String;
    _createtime = json['createtime'] as int;
    _iscommercial = json['iscommercial'] as int;
    _mail = json['mail'] as String;
    _position = json['position'] as String;
    _rank = json['rank'] as int;
    _rankbonus = json['rankbonus'] as int;
    _role = json['role'] as int;
    _updatetime = json['updatetime'] as int;
    _userid = json['userid'] as int;
    _username = json['username'] as String;
  }
  late String _age;
  late int _bonus;
  late String _country;
  late int _createtime;
  late int _iscommercial;
  late String _mail;
  late String _position;
  late int _rank;
  late int _rankbonus;
  late int _role;
  late int _updatetime;
  late int _userid;
  late String _username;

  String get age => _age;
  int get bonus => _bonus;
  String get country => _country;
  int get createtime => _createtime;
  int get iscommercial => _iscommercial;
  String get mail => _mail;
  String get position => _position;
  int get rank => _rank;
  int get rankbonus => _rankbonus;
  int get role => _role;
  int get updatetime => _updatetime;
  int get userid => _userid;
  String get username => _username;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['age'] = _age;
    map['bonus'] = _bonus;
    map['country'] = _country;
    map['createtime'] = _createtime;
    map['iscommercial'] = _iscommercial;
    map['mail'] = _mail;
    map['position'] = _position;
    map['rank'] = _rank;
    map['rankbonus'] = _rankbonus;
    map['role'] = _role;
    map['updatetime'] = _updatetime;
    map['userid'] = _userid;
    map['username'] = _username;
    return map;
  }
}
