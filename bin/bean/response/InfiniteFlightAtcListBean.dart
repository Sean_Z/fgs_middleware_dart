/// errorCode : 0
/// result : [{"frequencyId":"c2d7decc-2803-c905-5d88-81bc07626b1f","userId":"3f8b28bf-bbb1-4024-80ae-2a0ea9b30685","username":"Cameron","virtualOrganization":null,"airportName":"LEPA","type":1,"latitude":39.551575,"longitude":2.736811,"startTime":"2020-10-02 15:47:25Z"}]

class InfiniteFlightAtcListBean {
  late int errorCode;
  late List<InfiniteFlightAtcBeanResult> result;

  InfiniteFlightAtcListBean({required this.errorCode, required this.result});

  InfiniteFlightAtcListBean.fromJson(dynamic json) {
    errorCode = json['errorCode'] as int;
    if (json['result'] != null) {
      result = [];
      json['result'].forEach((v) {
        result.add(InfiniteFlightAtcBeanResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['errorCode'] = errorCode;
    map['result'] = result.map((v) => v.toJson()).toList();
    return map;
  }
}

/// frequencyId : "c2d7decc-2803-c905-5d88-81bc07626b1f"
/// userId : "3f8b28bf-bbb1-4024-80ae-2a0ea9b30685"
/// username : "Cameron"
/// virtualOrganization : null
/// airportName : "LEPA"
/// type : 1
/// latitude : 39.551575
/// longitude : 2.736811
/// startTime : "2020-10-02 15:47:25Z"

class InfiniteFlightAtcBeanResult {
  late String frequencyId;
  late String userId;
  late String username;
  late dynamic virtualOrganization;
  late String airportName;
  late int type;
  late double latitude;
  late double longitude;
  late String startTime;

  InfiniteFlightAtcBeanResult(
      {required this.frequencyId,
      required this.userId,
      required this.username,
      this.virtualOrganization,
      required this.airportName,
      required this.type,
      required this.latitude,
      required this.longitude,
      required this.startTime});

  InfiniteFlightAtcBeanResult.fromJson(dynamic json) {
    frequencyId = json['frequencyId'] as String;
    userId = json['userId'] as String;
    username = json['username'] as String;
    virtualOrganization = json['virtualOrganization'];
    airportName = (json['airportName'] ?? 'unknown') as String;
    type = json['type'] as int;
    latitude = double.parse(json['latitude'].toString());
    longitude = double.parse(json['longitude'].toString());
    startTime = json['startTime'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['frequencyId'] = frequencyId;
    map['userId'] = userId;
    map['username'] = username;
    map['virtualOrganization'] = virtualOrganization;
    map['airportName'] = airportName;
    map['type'] = type;
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    map['startTime'] = startTime;
    return map;
  }
}
