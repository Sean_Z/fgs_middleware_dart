/// message : "success!"
/// status : 0
/// nodeToken : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlNlYW5fWmhhbmciLCJwYXNzd29yZCI6InNlYW4xOTk3NTIyIiwiaWF0IjoxNjIzMzgwNjY4LCJleHAiOjYwNjQyMzM4MDY2OH0.nZa5QjG26QbuWdgYCq5aeToqG0wxD8lVprsgpW4b5fA"
/// accessToken : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJBNDNBNjQ3Qjc5QjFENzM3QTMyMEZCM0FEQzBDN0YyMiIsInVzZXJOYW1lIjoiU2Vhbl9aaGFuZyIsInJvbGUiOjIsInVzZXJBZ2VudCI6ImF4aW9zLzAuMjEuMSIsImV4cCI6MTYyMzM4MjQ2OCwibmJmIjoxNjIzMzgwNjY4fQ.ufuMqm3XYuMKTlNFcPcHNBiJfIS0xcjBYXP0_HIdtnM"
/// refreshToken : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJBNDNBNjQ3Qjc5QjFENzM3QTMyMEZCM0FEQzBDN0YyMiIsInVzZXJOYW1lIjoiU2Vhbl9aaGFuZyIsInJvbGUiOjIsInVzZXJBZ2VudCI6IiIsImV4cCI6MTYyMzk4NTQ2OCwibmJmIjoxNjIzMzgwNjY4fQ.T8fX1wZOtGjHB7OGzuHziWrkMPOxfQmD6HrGbuMwmZ4"
/// isCommercial : 1
/// position : "CTO"
/// username : "Sean_Zhang"
/// email : "zhang1074875355@gmail.com"
/// currentPosition : "Hangzhou of China"
/// authToken : ""

class LoginResponseBean {
  String? message;
  int? status;
  String? nodeToken;
  String? accessToken;
  String? refreshToken;
  int? isCommercial;
  String? position;
  String? username;
  String? email;
  String? currentPosition;
  String? authToken;

  LoginResponseBean(
      {this.message,
      this.status,
      this.nodeToken,
      this.accessToken,
      this.refreshToken,
      this.isCommercial,
      this.position,
      this.username,
      this.email,
      this.currentPosition,
      this.authToken});

  LoginResponseBean.fromJson(dynamic json) {
    message = json['message'] as String;
    status = json['status'] as int;
    nodeToken = (json['nodeToken'] ?? '') as String;
    accessToken = (json['accessToken'] ?? '') as String;
    refreshToken = (json['refreshToken'] ?? '') as String;
    isCommercial = (json['isCommercial'] ?? 1) as int;
    position = (json['position'] ?? '') as String;
    username = (json['username'] ?? '') as String;
    email = (json['email'] ?? '') as String;
    currentPosition = (json['currentPosition'] ?? '') as String;
    authToken = (json['authToken'] ?? '') as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['message'] = message;
    map['status'] = status;
    map['nodeToken'] = nodeToken;
    map['accessToken'] = accessToken;
    map['refreshToken'] = refreshToken;
    map['isCommercial'] = isCommercial;
    map['position'] = position;
    map['username'] = username;
    map['email'] = email;
    map['currentPosition'] = currentPosition;
    map['authToken'] = authToken;
    return map;
  }
}
