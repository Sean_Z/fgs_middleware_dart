class InfiniteFlightUserInfoBean {
  late int errorCode;
  late List<InfiniteFlightUserInfoResult> result;

  InfiniteFlightUserInfoBean({required this.errorCode, required this.result});

  InfiniteFlightUserInfoBean.fromJson(dynamic json) {
    errorCode = (json['errorCode'] ?? 1) as int;
    if (json['result'] != null) {
      result = [];
      json['result'].forEach((v) {
        result.add(InfiniteFlightUserInfoResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['errorCode'] = errorCode;
    map['result'] = result.map((v) => v.toJson()).toList();
    return map;
  }
}

class InfiniteFlightUserInfoResult {
  late int onlineFlights;
  late int violations;
  late int xp;
  late int landingCount;
  late int flightTime;
  late int atcOperations;
  late int atcRank;
  late int grade;
  late String hash;
  late ViolationCountByLevel violationCountByLevel;
  late List<int> roles;
  late String userId;
  late dynamic virtualOrganization;
  late String discourseUsername;
  late List<String> groups;
  late int errorCode;

  InfiniteFlightUserInfoResult(
      {required this.onlineFlights,
      required this.violations,
      required this.xp,
      required this.landingCount,
      required this.flightTime,
      required this.atcOperations,
      required this.atcRank,
      required this.grade,
      required this.hash,
      required this.violationCountByLevel,
      required this.roles,
      required this.userId,
      required this.virtualOrganization,
      required this.discourseUsername,
      required this.groups,
      required this.errorCode});

  InfiniteFlightUserInfoResult.fromJson(dynamic json) {
    onlineFlights = json['onlineFlights'] as int;
    violations = json['violations'] as int;
    xp = json['xp'] as int;
    landingCount = json['landingCount'] as int;
    flightTime = json['flightTime'] as int;
    atcOperations = json['atcOperations'] as int;
    atcRank = json['atcRank'] as int;
    grade = json['grade'] as int;
    hash = json['hash'] as String;
    violationCountByLevel = (json['violationCountByLevel'] != null ? ViolationCountByLevel.fromJson(json['violationCountByLevel']) : null)!;
    roles = json['roles'] != null ? (json['roles'] as List).cast() : [];
    userId = json['userId'] as String;
    virtualOrganization = json['virtualOrganization'];
    discourseUsername = (json['discourseUsername'] ?? 'unknown') as String;
    groups = json['groups'] != null ? (json['groups'] as List).cast() : [];
    errorCode = json['errorCode'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['onlineFlights'] = onlineFlights;
    map['violations'] = violations;
    map['xp'] = xp;
    map['landingCount'] = landingCount;
    map['flightTime'] = flightTime;
    map['atcOperations'] = atcOperations;
    map['atcRank'] = atcRank;
    map['grade'] = grade;
    map['hash'] = hash;
    map['violationCountByLevel'] = violationCountByLevel.toJson();
    map['roles'] = roles;
    map['userId'] = userId;
    map['virtualOrganization'] = virtualOrganization;
    map['discourseUsername'] = discourseUsername;
    map['groups'] = groups;
    map['errorCode'] = errorCode;
    return map;
  }
}

class ViolationCountByLevel {
  late int level1;
  late int level2;
  late int level3;

  ViolationCountByLevel({required this.level1, required this.level2, required this.level3});

  ViolationCountByLevel.fromJson(dynamic json) {
    level1 = json['level1'] as int;
    level2 = json['level2'] as int;
    level3 = json['level3'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['level1'] = level1;
    map['level2'] = level2;
    map['level3'] = level3;
    return map;
  }
}
