class FplIdRes {
  FplIdRes({
    required int id,
    required String fromICAO,
    required String toICAO,
    required String fromName,
    required String toName,
    required String flightNumber,
    required double distance,
    required int maxAltitude,
    required int waypoints,
    required int popularity,
    required String notes,
    required String encodedPolyline,
    required String createdAt,
    required String updatedAt,
    required List<String> tags,
    required User user,
  }) {
    _id = id;
    _fromICAO = fromICAO;
    _toICAO = toICAO;
    _fromName = fromName;
    _toName = toName;
    _flightNumber = flightNumber;
    _distance = distance;
    _maxAltitude = maxAltitude;
    _waypoints = waypoints;
    _popularity = popularity;
    _notes = notes;
    _encodedPolyline = encodedPolyline;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _tags = tags;
    _user = user;
  }

  FplIdRes.fromJson(dynamic json) {
    _id = json['id'];
    _fromICAO = json['fromICAO'];
    _toICAO = json['toICAO'];
    _fromName = json['fromName'];
    _toName = json['toName'];
    _flightNumber = json['flightNumber'];
    _distance = json['distance'];
    _maxAltitude = json['maxAltitude'];
    _waypoints = json['waypoints'];
    _popularity = json['popularity'];
    _notes = json['notes'];
    _encodedPolyline = json['encodedPolyline'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _tags = json['tags'] != null ? json['tags'].cast<String>() : [];
    _user = (json['user'] != null ? User.fromJson(json['user']) : null)!;
  }
  late int _id;
  late String _fromICAO;
  late String _toICAO;
  late String _fromName;
  late String _toName;
  late String _flightNumber;
  late double _distance;
  late int _maxAltitude;
  late int _waypoints;
  late int _popularity;
  late String _notes;
  late String _encodedPolyline;
  late String _createdAt;
  late String _updatedAt;
  late List<String> _tags;
  late User _user;
  FplIdRes copyWith({
    required int id,
    required String fromICAO,
    required String toICAO,
    required String fromName,
    required String toName,
    required String flightNumber,
    required double distance,
    required int maxAltitude,
    required int waypoints,
    required int popularity,
    required String notes,
    required String encodedPolyline,
    required String createdAt,
    required String updatedAt,
    required List<String> tags,
    required User user,
  }) =>
      FplIdRes(
        id: id,
        fromICAO: fromICAO,
        toICAO: toICAO,
        fromName: fromName,
        toName: toName,
        flightNumber: flightNumber,
        distance: distance,
        maxAltitude: maxAltitude,
        waypoints: waypoints,
        popularity: popularity,
        notes: notes,
        encodedPolyline: encodedPolyline,
        createdAt: createdAt,
        updatedAt: updatedAt,
        tags: tags,
        user: user,
      );
  int get id => _id;
  String get fromICAO => _fromICAO;
  String get toICAO => _toICAO;
  String get fromName => _fromName;
  String get toName => _toName;
  dynamic get flightNumber => _flightNumber;
  double get distance => _distance;
  int get maxAltitude => _maxAltitude;
  int get waypoints => _waypoints;
  int get popularity => _popularity;
  String get notes => _notes;
  String get encodedPolyline => _encodedPolyline;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  List<String> get tags => _tags;
  User get user => _user;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['fromICAO'] = _fromICAO;
    map['toICAO'] = _toICAO;
    map['fromName'] = _fromName;
    map['toName'] = _toName;
    map['flightNumber'] = _flightNumber;
    map['distance'] = _distance;
    map['maxAltitude'] = _maxAltitude;
    map['waypoints'] = _waypoints;
    map['popularity'] = _popularity;
    map['notes'] = _notes;
    map['encodedPolyline'] = _encodedPolyline;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['tags'] = _tags;
    map['user'] = _user.toJson();
    return map;
  }
}

class User {
  User({
    required int id,
    required String username,
    required String gravatarHash,
    required dynamic location,
  }) {
    _id = id;
    _username = username;
    _gravatarHash = gravatarHash;
    _location = location;
  }

  User.fromJson(dynamic json) {
    _id = json['id'];
    _username = json['username'];
    _gravatarHash = json['gravatarHash'];
    _location = json['location'];
  }
  late int _id;
  late String _username;
  late String _gravatarHash;
  late dynamic _location;
  User copyWith({
    required int id,
    required String username,
    required String gravatarHash,
    required dynamic location,
  }) =>
      User(
        id: id,
        username: username,
        gravatarHash: gravatarHash,
        location: location ?? _location,
      );
  int get id => _id;
  String get username => _username;
  String get gravatarHash => _gravatarHash;
  dynamic get location => _location;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['username'] = _username;
    map['gravatarHash'] = _gravatarHash;
    map['location'] = _location;
    return map;
  }
}
