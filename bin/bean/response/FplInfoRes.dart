class FplInfoRes {
  FplInfoRes({
    required int id,
    required String fromICAO,
    required String toICAO,
    required String fromName,
    required String toName,
    required dynamic flightNumber,
    required double distance,
    required int maxAltitude,
    required int waypoints,
    required int popularity,
    required dynamic notes,
    required String encodedPolyline,
    required String createdAt,
    required String updatedAt,
    required List<String> tags,
    required User user,
    required Route route,
  }) {
    _id = id;
    _fromICAO = fromICAO;
    _toICAO = toICAO;
    _fromName = fromName;
    _toName = toName;
    _flightNumber = flightNumber;
    _distance = distance;
    _maxAltitude = maxAltitude;
    _waypoints = waypoints;
    _popularity = popularity;
    _notes = notes;
    _encodedPolyline = encodedPolyline;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _tags = tags;
    _user = user;
    _route = route;
  }

  FplInfoRes.fromJson(dynamic json) {
    _id = json['id'];
    _fromICAO = json['fromICAO'];
    _toICAO = json['toICAO'];
    _fromName = json['fromName'];
    _toName = json['toName'];
    _flightNumber = json['flightNumber'];
    _distance = json['distance'];
    _maxAltitude = json['maxAltitude'];
    _waypoints = json['waypoints'];
    _popularity = json['popularity'];
    _notes = json['notes'];
    _encodedPolyline = json['encodedPolyline'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _tags = json['tags'] != null ? json['tags'].cast<String>() : [];
    _user = (json['user'] != null ? User.fromJson(json['user']) : null)!;
    _route = (json['route'] != null ? Route.fromJson(json['route']) : null)!;
  }
  late int _id;
  late String _fromICAO;
  late String _toICAO;
  late String _fromName;
  late String _toName;
  late dynamic _flightNumber;
  late double _distance;
  late int _maxAltitude;
  late int _waypoints;
  late int _popularity;
  late dynamic _notes;
  late String _encodedPolyline;
  late String _createdAt;
  late String _updatedAt;
  late List<String> _tags;
  late User _user;
  late Route _route;
  FplInfoRes copyWith({
    required int id,
    required String fromICAO,
    required String toICAO,
    required String fromName,
    required String toName,
    required dynamic flightNumber,
    required double distance,
    required int maxAltitude,
    required int waypoints,
    required int popularity,
    required dynamic notes,
    required String encodedPolyline,
    required String createdAt,
    required String updatedAt,
    required List<String> tags,
    required User user,
    required Route route,
  }) =>
      FplInfoRes(
        id: id,
        fromICAO: fromICAO,
        toICAO: toICAO,
        fromName: fromName,
        toName: toName,
        flightNumber: flightNumber,
        distance: distance,
        maxAltitude: maxAltitude,
        waypoints: waypoints,
        popularity: popularity,
        notes: notes,
        encodedPolyline: encodedPolyline,
        createdAt: createdAt,
        updatedAt: updatedAt,
        tags: tags,
        user: user,
        route: route,
      );
  int get id => _id;
  String get fromICAO => _fromICAO;
  String get toICAO => _toICAO;
  String get fromName => _fromName;
  String get toName => _toName;
  dynamic get flightNumber => _flightNumber;
  double get distance => _distance;
  int get maxAltitude => _maxAltitude;
  int get waypoints => _waypoints;
  int get popularity => _popularity;
  dynamic get notes => _notes;
  String get encodedPolyline => _encodedPolyline;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  List<String> get tags => _tags;
  User get user => _user;
  Route get route => _route;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['fromICAO'] = _fromICAO;
    map['toICAO'] = _toICAO;
    map['fromName'] = _fromName;
    map['toName'] = _toName;
    map['flightNumber'] = _flightNumber;
    map['distance'] = _distance;
    map['maxAltitude'] = _maxAltitude;
    map['waypoints'] = _waypoints;
    map['popularity'] = _popularity;
    map['notes'] = _notes;
    map['encodedPolyline'] = _encodedPolyline;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['tags'] = _tags;
    map['user'] = _user.toJson();
    map['route'] = _route.toJson();
    return map;
  }
}

class Route {
  Route({
    required List<Nodes> nodes,
  }) {
    _nodes = nodes;
  }

  Route.fromJson(dynamic json) {
    if (json['nodes'] != null) {
      _nodes = [];
      json['nodes'].forEach((v) {
        _nodes.add(Nodes.fromJson(v));
      });
    }
  }
  late List<Nodes> _nodes;
  Route copyWith({
    required List<Nodes> nodes,
  }) =>
      Route(
        nodes: nodes,
      );
  List<Nodes> get nodes => _nodes;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['nodes'] = _nodes.map((v) => v.toJson()).toList();
    return map;
  }
}

class Nodes {
  Nodes({
    required String type,
    required String ident,
    required String name,
    required double lat,
    required double lon,
    required int alt,
    required dynamic via,
  }) {
    _type = type;
    _ident = ident;
    _name = name;
    _lat = lat;
    _lon = lon;
    _alt = alt;
    _via = via;
  }

  Nodes.fromJson(dynamic json) {
    _type = json['type'];
    _ident = json['ident'];
    _name = json['name'];
    _lat = json['lat'];
    _lon = json['lon'];
    _alt = json['alt'];
    _via = json['via'];
  }
  late String _type;
  late String _ident;
  late String _name;
  late double _lat;
  late double _lon;
  late int _alt;
  late dynamic _via;
  Nodes copyWith({
    required String type,
    required String ident,
    required String name,
    required double lat,
    required double lon,
    required int alt,
    required dynamic via,
  }) =>
      Nodes(
        type: type,
        ident: ident,
        name: name,
        lat: lat,
        lon: lon,
        alt: alt,
        via: via,
      );
  String get type => _type;
  String get ident => _ident;
  String get name => _name;
  double get lat => _lat;
  double get lon => _lon;
  int get alt => _alt;
  dynamic get via => _via;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['type'] = _type;
    map['ident'] = _ident;
    map['name'] = _name;
    map['lat'] = _lat;
    map['lon'] = _lon;
    map['alt'] = _alt;
    map['via'] = _via;
    return map;
  }
}

class User {
  User({
    required int id,
    required String username,
    required String gravatarHash,
    required dynamic location,
  }) {
    _id = id;
    _username = username;
    _gravatarHash = gravatarHash;
    _location = location;
  }

  User.fromJson(dynamic json) {
    _id = json['id'];
    _username = json['username'];
    _gravatarHash = json['gravatarHash'];
    _location = json['location'];
  }
  late int _id;
  late String _username;
  late String _gravatarHash;
  late dynamic _location;
  User copyWith({
    required int id,
    required String username,
    required String gravatarHash,
    required dynamic location,
  }) =>
      User(
        id: id,
        username: username,
        gravatarHash: gravatarHash,
        location: location,
      );
  int get id => _id;
  String get username => _username;
  String get gravatarHash => _gravatarHash;
  dynamic get location => _location;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['username'] = _username;
    map['gravatarHash'] = _gravatarHash;
    map['location'] = _location;
    return map;
  }
}
