/// dewpoint : 13
/// qnh : 30
/// temperature : 20
/// time : "2021-07-29 13:00"
/// visibility : 32000
/// windSpeed : 5

class AirportWeatherBean {
  double? _dewpoint;
  double? _qnh;
  double? _temperature;
  String? _time;
  double? _visibility;
  double? _windSpeed;

  double? get dewpoint => _dewpoint;
  double? get qnh => _qnh;
  double? get temperature => _temperature;
  String? get time => _time;
  double? get visibility => _visibility;
  double? get windSpeed => _windSpeed;

  AirportWeatherBean({double? dewpoint, double? qnh, double? temperature, String? time, double? visibility, double? windSpeed}) {
    _dewpoint = dewpoint;
    _qnh = qnh;
    _temperature = temperature;
    _time = time;
    _visibility = visibility;
    _windSpeed = windSpeed;
  }

  AirportWeatherBean.fromJson(dynamic json) {
    _dewpoint = double.parse(json['dewpoint'].toString());
    _qnh = double.parse(json['qnh'].toString());
    _temperature = double.parse(json['temperature'].toString());
    _time = json['time'] as String;
    _visibility = double.parse(json['visibility'].toString());
    _windSpeed = double.parse(json['windSpeed'].toString());
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['dewpoint'] = _dewpoint;
    map['qnh'] = _qnh;
    map['temperature'] = _temperature;
    map['time'] = _time;
    map['visibility'] = _visibility;
    map['windSpeed'] = _windSpeed;
    return map;
  }
}
