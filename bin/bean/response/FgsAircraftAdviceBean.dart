/// createtime : 1631069780000
/// cruisingspeed : 0.78
/// id : 1
/// landingflaps : "Full"
/// landingtrim : "30"
/// maxcargo : "8408"
/// maxpassenger : "132"
/// mlw : "57000"
/// model : "Airbus A318"
/// modifytime : 1631069826000
/// mtow : "68000"
/// rangetime : "7:56"
/// remark : "Take off Flaps setting is different from the SOP"
/// takeoffflaps : "1~2 ~1+F"
/// takeofftrim : "30"

class FgsAircraftAdviceBean {
  late int _createtime;
  late double _cruisingspeed;
  late int _id;
  late String _landingflaps;
  late String _landingtrim;
  late String _maxcargo;
  late String _maxpassenger;
  late String _mlw;
  late String _model;
  late int _modifytime;
  late String _mtow;
  late String _rangetime;
  late String _remark;
  late String _takeoffflaps;
  late String _takeofftrim;

  int get createtime => _createtime;
  double get cruisingspeed => _cruisingspeed;
  int get id => _id;
  String get landingflaps => _landingflaps;
  String get landingtrim => _landingtrim;
  String get maxcargo => _maxcargo;
  String get maxpassenger => _maxpassenger;
  String get mlw => _mlw;
  String get model => _model;
  int get modifytime => _modifytime;
  String get mtow => _mtow;
  String get rangetime => _rangetime;
  String get remark => _remark;
  String get takeoffflaps => _takeoffflaps;
  String get takeofftrim => _takeofftrim;

  FgsAircraftAdviceBean(
      {required int createtime,
      bool? isExpand,
      required double cruisingspeed,
      required int id,
      required String landingflaps,
      required String landingtrim,
      required String maxcargo,
      required String maxpassenger,
      required String mlw,
      required String model,
      required int modifytime,
      required String mtow,
      required String rangetime,
      required String remark,
      required String takeoffflaps,
      required String takeofftrim}) {
    _createtime = createtime;
    _cruisingspeed = cruisingspeed;
    _id = id;
    _landingflaps = landingflaps;
    _landingtrim = landingtrim;
    _maxcargo = maxcargo;
    _maxpassenger = maxpassenger;
    _mlw = mlw;
    _model = model;
    _modifytime = modifytime;
    _mtow = mtow;
    _rangetime = rangetime;
    _remark = remark;
    _takeoffflaps = takeoffflaps;
    _takeofftrim = takeofftrim;
  }

  FgsAircraftAdviceBean.fromJson(dynamic json) {
    _createtime = json['createtime'] as int;
    _cruisingspeed = double.parse(json['cruisingspeed'].toString());
    _id = json['id'] as int;
    _landingflaps = json['landingflaps'] as String;
    _landingtrim = json['landingtrim'] as String;
    _maxcargo = json['maxcargo'] as String;
    _maxpassenger = json['maxpassenger'] as String;
    _mlw = json['mlw'] as String;
    _model = json['model'] as String;
    _modifytime = json['modifytime'] as int;
    _mtow = json['mtow'] as String;
    _rangetime = json['rangetime'] as String;
    _remark = json['remark'] as String;
    _takeoffflaps = json['takeoffflaps'] as String;
    _takeofftrim = json['takeofftrim'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['createtime'] = _createtime;
    map['cruisingspeed'] = _cruisingspeed;
    map['id'] = _id;
    map['landingflaps'] = _landingflaps;
    map['landingtrim'] = _landingtrim;
    map['maxcargo'] = _maxcargo;
    map['maxpassenger'] = _maxpassenger;
    map['mlw'] = _mlw;
    map['model'] = _model;
    map['modifytime'] = _modifytime;
    map['mtow'] = _mtow;
    map['rangetime'] = _rangetime;
    map['remark'] = _remark;
    map['takeoffflaps'] = _takeoffflaps;
    map['takeofftrim'] = _takeofftrim;
    return map;
  }
}
