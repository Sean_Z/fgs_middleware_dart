class FlightCollectionRes {
  FlightCollectionRes({
    required int totalAircraft,
    required List<KvClass> kvClass,
  }) {
    _totalAircraft = totalAircraft;
    _kvClass = kvClass;
  }

  FlightCollectionRes.fromJson(dynamic json) {
    _totalAircraft = json['totalAircraft'];
    if (json['kvClass'] != null) {
      _kvClass = [];
      json['kvClass'].forEach((v) {
        _kvClass.add(KvClass.fromJson(v));
      });
    }
  }
  late int _totalAircraft;
  late List<KvClass> _kvClass;
  FlightCollectionRes copyWith({
    required int totalAircraft,
    required List<KvClass> kvClass,
  }) =>
      FlightCollectionRes(
        totalAircraft: totalAircraft,
        kvClass: kvClass,
      );
  int? get totalAircraft => _totalAircraft;
  List<KvClass> get kvClass => _kvClass;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['totalAircraft'] = _totalAircraft;
    map['kvClass'] = _kvClass.map((v) => v.toJson()).toList();
    return map;
  }
}

class KvClass {
  KvClass({
    String? name,
    int? value,
  }) {
    _name = name;
    _value = value;
  }

  KvClass.fromJson(dynamic json) {
    _name = json['name'];
    _value = json['value'];
  }
  String? _name;
  int? _value;
  KvClass copyWith({
    String? name,
    int? value,
  }) =>
      KvClass(
        name: name ?? _name,
        value: value ?? _value,
      );
  String? get name => _name;
  int? get value => _value;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['value'] = _value;
    return map;
  }
}
