/// status : 0
/// message : "log successfully!"

class CommonResponse {
  late int status;
  late String message;

  CommonResponse({required this.status, required this.message});

  CommonResponse.fromJson(dynamic json) {
    status = json['status'] as int;
    message = json['message'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    return map;
  }
}
