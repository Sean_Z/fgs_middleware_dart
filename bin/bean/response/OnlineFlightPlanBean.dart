class OnlineFlightPlanBean {
  late String _routeInfo;
  late String _departure;
  late String _arrival;
  late DeparturePosition _departurePosition;
  late ArrivalPosition _arrivalPosition;
  late List<WholeRangePosition> _wholeRangePosition;

  String get routeInfo => _routeInfo;
  String get departure => _departure;
  String get arrival => _arrival;
  DeparturePosition get departurePosition => _departurePosition;
  ArrivalPosition get arrivalPosition => _arrivalPosition;
  List<WholeRangePosition> get wholeRangePosition => _wholeRangePosition;

  OnlineFlightPlanBean(
      {required String routeInfo,
      required String departure,
      required String arrival,
      required DeparturePosition departurePosition,
      required ArrivalPosition arrivalPosition,
      required List<WholeRangePosition> wholeRangePosition}) {
    _routeInfo = routeInfo;
    _departure = departure;
    _arrival = arrival;
    _departurePosition = departurePosition;
    _arrivalPosition = arrivalPosition;
    _wholeRangePosition = wholeRangePosition;
  }

  OnlineFlightPlanBean.fromJson(dynamic json) {
    _routeInfo = (json['routerInfo'] ?? '') as String;
    _departure = json['departure'] as String;
    _arrival = json['arrival'] as String;
    _departurePosition = (json['departurePosition'] != null ? DeparturePosition.fromJson(json['departurePosition']) : null)!;
    _arrivalPosition = (json['arrivalPosition'] != null ? ArrivalPosition.fromJson(json['arrivalPosition']) : null)!;
    if (json['wholeRangePosition'] != null) {
      _wholeRangePosition = [];
      json['wholeRangePosition'].forEach((v) {
        _wholeRangePosition.add(WholeRangePosition.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['routeInfo'] = _routeInfo;
    map['departure'] = _departure;
    map['arrival'] = _arrival;
    map['departurePosition'] = _departurePosition.toJson();
    map['arrivalPosition'] = _arrivalPosition.toJson();
    map['wholeRangePosition'] = _wholeRangePosition.map((v) => v.toJson()).toList();
    return map;
  }
}

class WholeRangePosition {
  late double _latitude;
  late double _longitude;

  double get latitude => _latitude;
  double get longitude => _longitude;

  WholeRangePosition({required double latitude, required double longitude}) {
    _latitude = latitude;
    _longitude = longitude;
  }

  WholeRangePosition.fromJson(dynamic json) {
    _latitude = double.parse(json['latitude'].toString());
    _longitude = double.parse(json['longitude'].toString());
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    return map;
  }
}

class ArrivalPosition {
  late double _latitude;
  late double _longitude;
  late double _altitude;

  double get latitude => _latitude;
  double get longitude => _longitude;
  double get altitude => _altitude;

  ArrivalPosition({required double latitude, required double longitude, required double altitude}) {
    _latitude = latitude;
    _longitude = longitude;
    _altitude = altitude;
  }

  ArrivalPosition.fromJson(dynamic json) {
    _latitude = double.parse(json['latitude'].toString());
    _longitude = double.parse(json['longitude'].toString());
    _altitude = double.parse((json['altitude'] ?? 0.0).toString());
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    map['altitude'] = _altitude;
    return map;
  }
}

class DeparturePosition {
  late double _latitude;
  late double _longitude;
  late double _altitude;

  double get latitude => _latitude;
  double get longitude => _longitude;
  double get altitude => _altitude;

  DeparturePosition({required double latitude, required double longitude, required double altitude}) {
    _latitude = latitude;
    _longitude = longitude;
    _altitude = altitude;
  }

  DeparturePosition.fromJson(dynamic json) {
    _latitude = double.parse(json['latitude'].toString());
    _longitude = double.parse(json['longitude'].toString());
    _altitude = double.parse((json['altitude'] ?? 0.0).toString());
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    map['altitude'] = _altitude;
    return map;
  }
}
