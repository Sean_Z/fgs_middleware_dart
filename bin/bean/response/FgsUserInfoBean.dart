/// flightInfoBean : {"totalFlight":18,"totalFlightTime":10033,"totalFlightRange":80038,"favoriteAircraft":"Boeing 787-9","ffpInfo":[{"aircraft":"Boeing 787-9","livery":"WestJet","flighttime":"715"},{"aircraft":"Airbus A319","livery":"Air China","flighttime":"64"},{"aircraft":"Boeing 737-800","livery":"American Airlines","flighttime":"151"},{"aircraft":"CRJ-900","livery":"China Express Airlines","flighttime":"149"},{"aircraft":"Airbus A330-300","livery":"Airbus","flighttime":"761"},{"aircraft":"Boeing 777-200LR","livery":"Boeing","flighttime":"827"},{"aircraft":"Airbus A321","livery":"Iberia","flighttime":"101"},{"aircraft":"Boeing 777-300ER","livery":"Singapore","flighttime":"733"},{"aircraft":"Boeing 787-8","livery":"Aeromexico","flighttime":"734"},{"aircraft":"A359","livery":"Air Canada","flighttime":"900"},{"aircraft":"Boeing 747-400","livery":"KLM - 2015","flighttime":"549"},{"aircraft":"Airbus A321","livery":"Qatar Airways","flighttime":"494"},{"aircraft":"Airbus A320","livery":"Lufthansa 2018","flighttime":"62"},{"aircraft":"Airbus A350","livery":"Malaysia Airlines","flighttime":"1164"},{"aircraft":"Airbus A350","livery":"Infinite Flight - 10 Year Anniversary","flighttime":"1413"}],"favoriteAirline":"WestJet"}
/// userBean : {"country":"Hangzhou of China","createtime":1606836079000,"role":2,"mail":"zhang1074875355@gmail.com","wechatOpenId":"","bonus":10256,"phonenum":"+8613375742719","userid":82,"githubUsername":"","password":"","isvaild":0,"iscommercial":1,"rank":1,"position":"FGS Developer","rankbonus":1021,"updatetime":1627674182000,"age":"21-25","username":"Sean_Zhang"}

class FgsUserInfoBean {
  late FlightInfoBean flightInfoBean;
  UserBean? userBean;

  FgsUserInfoBean({required this.flightInfoBean, this.userBean});

  FgsUserInfoBean.fromJson(dynamic json) {
    flightInfoBean = FlightInfoBean.fromJson(json['flightInfoBean']);
    userBean = json['userBean'] != null ? UserBean.fromJson(json['userBean']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['flightInfoBean'] = flightInfoBean.toJson();
    if (userBean != null) {
      map['userBean'] = userBean?.toJson();
    }
    return map;
  }
}

/// country : "Hangzhou of China"
/// createtime : 1606836079000
/// role : 2
/// mail : "zhang1074875355@gmail.com"
/// wechatOpenId : ""
/// bonus : 10256
/// phonenum : "+8613375742719"
/// userid : 82
/// githubUsername : ""
/// password : ""
/// isvaild : 0
/// iscommercial : 1
/// rank : 1
/// position : "FGS Developer"
/// rankbonus : 1021
/// updatetime : 1627674182000
/// age : "21-25"
/// username : "Sean_Zhang"

class UserBean {
  String? _country;
  int? _createtime;
  int? _role;
  String? _mail;
  String? _wechatOpenId;
  int? _bonus;
  String? _phonenum;
  int? _userid;
  String? _githubUsername;
  String? _password;
  int? _isvaild;
  int? _iscommercial;
  int? _rank;
  String? _position;
  int? _rankbonus;
  int? _updatetime;
  String? _age;
  String? _username;

  String? get country => _country;
  int? get createtime => _createtime;
  int? get role => _role;
  String? get mail => _mail;
  String? get wechatOpenId => _wechatOpenId;
  int? get bonus => _bonus;
  String? get phonenum => _phonenum;
  int? get userid => _userid;
  String? get githubUsername => _githubUsername;
  String? get password => _password;
  int? get isvaild => _isvaild;
  int? get iscommercial => _iscommercial;
  int? get rank => _rank;
  String? get position => _position;
  int? get rankbonus => _rankbonus;
  int? get updatetime => _updatetime;
  String? get age => _age;
  String? get username => _username;

  UserBean(
      {String? country,
      int? createtime,
      int? role,
      String? mail,
      String? wechatOpenId,
      int? bonus,
      String? phonenum,
      int? userid,
      String? githubUsername,
      String? password,
      int? isvaild,
      int? iscommercial,
      int? rank,
      String? position,
      int? rankbonus,
      int? updatetime,
      String? age,
      String? username}) {
    _country = country;
    _createtime = createtime;
    _role = role;
    _mail = mail;
    _wechatOpenId = wechatOpenId;
    _bonus = bonus;
    _phonenum = phonenum;
    _userid = userid;
    _githubUsername = githubUsername;
    _password = password;
    _isvaild = isvaild;
    _iscommercial = iscommercial;
    _rank = rank;
    _position = position;
    _rankbonus = rankbonus;
    _updatetime = updatetime;
    _age = age;
    _username = username;
  }

  UserBean.fromJson(dynamic json) {
    _country = json['country'] as String;
    _createtime = json['createtime'] as int;
    _role = json['role'] as int;
    _mail = json['mail'] as String;
    _wechatOpenId = json['wechatOpenId'] as String;
    _bonus = json['bonus'] as int;
    _phonenum = (json['phonenum'] ?? '') as String;
    _userid = json['userid'] as int;
    _githubUsername = (json['githubUsername'] ?? '') as String;
    _password = json['password'] as String;
    _isvaild = json['isvaild'] as int;
    _iscommercial = json['iscommercial'] as int;
    _rank = json['rank'] as int;
    _position = json['position'] as String;
    _rankbonus = json['rankbonus'] as int;
    _updatetime = json['updatetime'] as int;
    _age = json['age'] as String;
    _username = json['username'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['country'] = _country;
    map['createtime'] = _createtime;
    map['role'] = _role;
    map['mail'] = _mail;
    map['wechatOpenId'] = _wechatOpenId;
    map['bonus'] = _bonus;
    map['phonenum'] = _phonenum;
    map['userid'] = _userid;
    map['githubUsername'] = _githubUsername;
    map['password'] = _password;
    map['isvaild'] = _isvaild;
    map['iscommercial'] = _iscommercial;
    map['rank'] = _rank;
    map['position'] = _position;
    map['rankbonus'] = _rankbonus;
    map['updatetime'] = _updatetime;
    map['age'] = _age;
    map['username'] = _username;
    return map;
  }
}

/// totalFlight : 18
/// totalFlightTime : 10033
/// totalFlightRange : 80038
/// favoriteAircraft : "Boeing 787-9"
/// ffpInfo : [{"aircraft":"Boeing 787-9","livery":"WestJet","flighttime":"715"},{"aircraft":"Airbus A319","livery":"Air China","flighttime":"64"},{"aircraft":"Boeing 737-800","livery":"American Airlines","flighttime":"151"},{"aircraft":"CRJ-900","livery":"China Express Airlines","flighttime":"149"},{"aircraft":"Airbus A330-300","livery":"Airbus","flighttime":"761"},{"aircraft":"Boeing 777-200LR","livery":"Boeing","flighttime":"827"},{"aircraft":"Airbus A321","livery":"Iberia","flighttime":"101"},{"aircraft":"Boeing 777-300ER","livery":"Singapore","flighttime":"733"},{"aircraft":"Boeing 787-8","livery":"Aeromexico","flighttime":"734"},{"aircraft":"A359","livery":"Air Canada","flighttime":"900"},{"aircraft":"Boeing 747-400","livery":"KLM - 2015","flighttime":"549"},{"aircraft":"Airbus A321","livery":"Qatar Airways","flighttime":"494"},{"aircraft":"Airbus A320","livery":"Lufthansa 2018","flighttime":"62"},{"aircraft":"Airbus A350","livery":"Malaysia Airlines","flighttime":"1164"},{"aircraft":"Airbus A350","livery":"Infinite Flight - 10 Year Anniversary","flighttime":"1413"}]
/// favoriteAirline : "WestJet"

class FlightInfoBean {
  int? _totalFlight;
  int? _totalFlightTime;
  int? _totalFlightRange;
  String? _favoriteAircraft;
  List<FfpInfo>? _ffpInfo;
  String? _favoriteAirline;

  int? get totalFlight => _totalFlight;
  int? get totalFlightTime => _totalFlightTime;
  int? get totalFlightRange => _totalFlightRange;
  String? get favoriteAircraft => _favoriteAircraft;
  List<FfpInfo>? get ffpInfo => _ffpInfo;
  String? get favoriteAirline => _favoriteAirline;

  FlightInfoBean({int? totalFlight, int? totalFlightTime, int? totalFlightRange, String? favoriteAircraft, List<FfpInfo>? ffpInfo, String? favoriteAirline}) {
    _totalFlight = totalFlight;
    _totalFlightTime = totalFlightTime;
    _totalFlightRange = totalFlightRange;
    _favoriteAircraft = favoriteAircraft;
    _ffpInfo = ffpInfo;
    _favoriteAirline = favoriteAirline;
  }

  FlightInfoBean.fromJson(dynamic json) {
    _totalFlight = (json['totalFlight'] ?? 0) as int;
    _totalFlightTime = (json['totalFlightTime'] ?? 0.0) as int;
    _totalFlightRange = (json['totalFlightRange'] ?? 0.0) as int;
    _favoriteAircraft = (json['favoriteAircraft'] ?? 'unknown') as String;
    if (json['ffpInfo'] != null) {
      _ffpInfo = [];
      json['ffpInfo'].forEach((v) {
        _ffpInfo?.add(FfpInfo.fromJson(v));
      });
    }
    _favoriteAirline = json['favoriteAirline'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['totalFlight'] = _totalFlight;
    map['totalFlightTime'] = _totalFlightTime;
    map['totalFlightRange'] = _totalFlightRange;
    map['favoriteAircraft'] = _favoriteAircraft;
    if (_ffpInfo != null) {
      map['ffpInfo'] = _ffpInfo?.map((v) => v.toJson()).toList();
    }
    map['favoriteAirline'] = _favoriteAirline;
    return map;
  }
}

/// aircraft : "Boeing 787-9"
/// livery : "WestJet"
/// flighttime : "715"

class FfpInfo {
  String? _aircraft;
  String? _livery;
  String? _flighttime;

  String? get aircraft => _aircraft;
  String? get livery => _livery;
  String? get flighttime => _flighttime;

  FfpInfo({String? aircraft, String? livery, String? flighttime}) {
    _aircraft = aircraft;
    _livery = livery;
    _flighttime = flighttime;
  }

  FfpInfo.fromJson(dynamic json) {
    _aircraft = json['aircraft'] as String;
    _livery = json['livery'] as String;
    _flighttime = json['flighttime'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['aircraft'] = _aircraft;
    map['livery'] = _livery;
    map['flighttime'] = _flighttime;
    return map;
  }
}
