class FlightLogListBean {
  late int endRow;
  late bool hasNextPage;
  late bool hasPreviousPage;
  late bool isFirstPage;
  late bool isLastPage;
  late List<LogList> list;
  late int navigateFirstPage;
  late int navigateLastPage;
  late int navigatePages;
  late List<int> navigatepageNums;
  late int nextPage;
  late int pageNum;
  late int pageSize;
  late int pages;
  late int prePage;
  late int size;
  late int startRow;
  late int total;

  FlightLogListBean(
      {required this.endRow,
      required this.hasNextPage,
      required this.hasPreviousPage,
      required this.isFirstPage,
      required this.isLastPage,
      required this.list,
      required this.navigateFirstPage,
      required this.navigateLastPage,
      required this.navigatePages,
      required this.navigatepageNums,
      required this.nextPage,
      required this.pageNum,
      required this.pageSize,
      required this.pages,
      required this.prePage,
      required this.size,
      required this.startRow,
      required this.total});

  FlightLogListBean.fromJson(dynamic json) {
    endRow = json['endRow'] as int;
    hasNextPage = json['hasNextPage'] as bool;
    hasPreviousPage = json['hasPreviousPage'] as bool;
    isFirstPage = json['isFirstPage'] as bool;
    isLastPage = json['isLastPage'] as bool;
    if (json['list'] != null) {
      list = [];
      json['list'].forEach((v) {
        list.add(LogList.fromJson(v));
      });
    }
    navigateFirstPage = json['navigateFirstPage'] as int;
    navigateLastPage = json['navigateLastPage'] as int;
    navigatePages = json['navigatePages'] as int;
    if (json['navigatepageNums'] != null) {
      navigatepageNums = [];
      json['navigatepageNums'].forEach((v) {
        navigatepageNums.add(v as int);
      });
    }
    nextPage = json['nextPage'] as int;
    pageNum = json['pageNum'] as int;
    pageSize = json['pageSize'] as int;
    pages = json['pages'] as int;
    prePage = json['prePage'] as int;
    size = json['size'] as int;
    startRow = json['startRow'] as int;
    total = json['total'] as int;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['endRow'] = endRow;
    map['hasNextPage'] = hasNextPage;
    map['hasPreviousPage'] = hasPreviousPage;
    map['isFirstPage'] = isFirstPage;
    map['isLastPage'] = isLastPage;
    map['list'] = list.map((v) => v.toJson()).toList();
    map['navigateFirstPage'] = navigateFirstPage;
    map['navigateLastPage'] = navigateLastPage;
    map['navigatePages'] = navigatePages;
    map['navigatepageNums'] = navigatepageNums;
    map['nextPage'] = nextPage;
    map['pageNum'] = pageNum;
    map['pageSize'] = pageSize;
    map['pages'] = pages;
    map['prePage'] = prePage;
    map['size'] = size;
    map['startRow'] = startRow;
    map['total'] = total;
    return map;
  }
}

class LogList {
  late String aircraft;
  late String arrival;
  late String arrivalairportname;
  late String arrivaltime;
  late int createdate;
  late String departure;
  late String departureairportname;
  late String departuretime;
  late int distance;
  late String flightid;
  late String flightnumber;
  late String flighttime;
  late String livery;
  late int logid;
  late int modifydate;
  late String pilot;
  late String route;
  late String boardingtime;

  LogList(
      {required this.aircraft,
      required this.arrival,
      required this.arrivalairportname,
      required this.arrivaltime,
      required this.createdate,
      required this.departure,
      required this.departureairportname,
      required this.departuretime,
      required this.distance,
      required this.flightid,
      required this.flightnumber,
      required this.flighttime,
      required this.livery,
      required this.logid,
      required this.modifydate,
      required this.pilot,
      required this.route,
      required this.boardingtime});

  LogList.fromJson(dynamic json) {
    aircraft = json['aircraft'] as String;
    arrival = json['arrival'] as String;
    arrivalairportname = json['arrivalairportname'] as String;
    arrivaltime = json['arrivaltime'] as String;
    createdate = json['createdate'] as int;
    departure = json['departure'] as String;
    departureairportname = json['departureairportname'] as String;
    departuretime = json['departuretime'] as String;
    distance = json['distance'] as int;
    flightid = json['flightid'] as String;
    flightnumber = json['flightnumber'] as String;
    flighttime = json['flighttime'] as String;
    livery = json['livery'] as String;
    logid = json['logid'] as int;
    modifydate = json['modifydate'] as int;
    pilot = json['pilot'] as String;
    route = json['route'] as String;
    boardingtime = json['boardingtime'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['aircraft'] = aircraft;
    map['arrival'] = arrival;
    map['arrivalairportname'] = arrivalairportname;
    map['arrivaltime'] = arrivaltime;
    map['createdate'] = createdate;
    map['departure'] = departure;
    map['departureairportname'] = departureairportname;
    map['departuretime'] = departuretime;
    map['distance'] = distance;
    map['flightid'] = flightid;
    map['flightnumber'] = flightnumber;
    map['flighttime'] = flighttime;
    map['livery'] = livery;
    map['logid'] = logid;
    map['modifydate'] = modifydate;
    map['pilot'] = pilot;
    map['route'] = route;
    map['boardingtime'] = boardingtime;
    return map;
  }
}
