/// errorCode : 0
/// result : [{"username":"Cameron","callsign":"EC-CAM","latitude":30.123479009207056,"longitude":31.413340256981044,"altitude":597.8003749797689,"speed":185.3844049009562,"verticalSpeed":2167.31591796875,"track":162.13836669921875,"lastReport":"2020-10-02 00:46:19Z","flightId":"348d1ba8-1e60-48ca-8278-42f019147de8","userId":"3f8b28bf-bbb1-4024-80ae-2a0ea9b30685","aircraftId":"de510d3d-04f8-46e0-8d65-55b888f33129","liveryId":"c875c0e9-19c2-420d-8fb4-32c151bd797c","heading":159.33542,"virtualOrganization":"IFATC [IFATC]"}]

class InfiniteFlightGetAllFlightsBean {
  late int _errorCode;
  late List<InfiniteFlightAllFlightsResult> _result;

  int get errorCode => _errorCode;
  List<InfiniteFlightAllFlightsResult> get result => _result;

  InfiniteFlightGetAllFlightsBean({required int errorCode, required List<InfiniteFlightAllFlightsResult> result}) {
    _errorCode = errorCode;
    _result = result;
  }

  InfiniteFlightGetAllFlightsBean.fromJson(dynamic json) {
    _errorCode = json['errorCode'] as int;
    if (json['result'] != null) {
      _result = [];
      json['result'].forEach((v) {
        _result.add(InfiniteFlightAllFlightsResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['errorCode'] = _errorCode;
    map['result'] = _result.map((v) => v.toJson()).toList();
    return map;
  }
}

/// username : "Cameron"
/// callsign : "EC-CAM"
/// latitude : 30.123479009207056
/// longitude : 31.413340256981044
/// altitude : 597.8003749797689
/// speed : 185.3844049009562
/// verticalSpeed : 2167.31591796875
/// track : 162.13836669921875
/// lastReport : "2020-10-02 00:46:19Z"
/// flightId : "348d1ba8-1e60-48ca-8278-42f019147de8"
/// userId : "3f8b28bf-bbb1-4024-80ae-2a0ea9b30685"
/// aircraftId : "de510d3d-04f8-46e0-8d65-55b888f33129"
/// liveryId : "c875c0e9-19c2-420d-8fb4-32c151bd797c"
/// heading : 159.33542
// ignore: comment_references
/// virtualOrganization : "IFATC [IFATC]"

class InfiniteFlightAllFlightsResult {
  late String _username;
  late String _callsign;
  late double _latitude;
  late double _longitude;
  late double _altitude;
  late double _speed;
  late double _verticalSpeed;
  late double _track;
  late String _lastReport;
  late String _flightId;
  late String _userId;
  late String _aircraftId;
  late String _liveryId;
  late double _heading;
  late String _virtualOrganization;

  String get username => _username;
  String get callsign => _callsign;
  double get latitude => _latitude;
  double get longitude => _longitude;
  double get altitude => _altitude;
  double get speed => _speed;
  double get verticalSpeed => _verticalSpeed;
  double get track => _track;
  String get lastReport => _lastReport;
  String get flightId => _flightId;
  String get userId => _userId;
  String get aircraftId => _aircraftId;
  String get liveryId => _liveryId;
  double get heading => _heading;
  String get virtualOrganization => _virtualOrganization;

  InfiniteFlightAllFlightsResult(
      {required String username,
      required String callsign,
      required double latitude,
      required double longitude,
      required double altitude,
      required double speed,
      required double verticalSpeed,
      required double track,
      required String lastReport,
      required String flightId,
      required String userId,
      required String aircraftId,
      required String liveryId,
      required double heading,
      required String virtualOrganization}) {
    _username = username;
    _callsign = callsign;
    _latitude = latitude;
    _longitude = longitude;
    _altitude = altitude;
    _speed = speed;
    _verticalSpeed = verticalSpeed;
    _track = track;
    _lastReport = lastReport;
    _flightId = flightId;
    _userId = userId;
    _aircraftId = aircraftId;
    _liveryId = liveryId;
    _heading = heading;
    _virtualOrganization = virtualOrganization;
  }

  InfiniteFlightAllFlightsResult.fromJson(dynamic json) {
    _username = (json['username'] ?? 'unknown') as String;
    _callsign = json['callsign'] as String;
    _latitude = double.parse(json['latitude'].toString());
    _longitude = double.parse(json['longitude'].toString());
    _altitude = double.parse(json['altitude'].toString());
    _speed = double.parse(json['speed'].toString());
    _verticalSpeed = double.parse(json['verticalSpeed'].toString());
    _track = double.parse(json['track'].toString());
    _lastReport = json['lastReport'] as String;
    _flightId = json['flightId'] as String;
    _userId = json['userId'] as String;
    _aircraftId = json['aircraftId'] as String;
    _liveryId = json['liveryId'] as String;
    _heading = double.parse(json['heading'].toString());
    _virtualOrganization = (json['virtualOrganization'] ?? '') as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['username'] = _username;
    map['callsign'] = _callsign;
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    map['altitude'] = _altitude;
    map['speed'] = _speed;
    map['verticalSpeed'] = _verticalSpeed;
    map['track'] = _track;
    map['lastReport'] = _lastReport;
    map['flightId'] = _flightId;
    map['userId'] = _userId;
    map['aircraftId'] = _aircraftId;
    map['liveryId'] = _liveryId;
    map['heading'] = _heading;
    map['virtualOrganization'] = _virtualOrganization;
    return map;
  }
}
