/// status : "1"
/// regeocode : {"addressComponent":{"city":"杭州市","province":"浙江省","adcode":"330106","district":"西湖区","towncode":"330106109000","streetNumber":{"number":"40号","location":"120.056986,30.319934","direction":"东南","distance":"91.2289","street":"振中路"},"country":"中国","township":"三墩镇","citycode":"0571"},"formatted_address":"浙江省杭州市西湖区三墩镇灯彩街"}
/// info : "OK"
/// infocode : "10000"

class LatLngDecodeBean {
  String? _status;
  Regeocode? _regeocode;
  String? _info;
  String? _infocode;

  String? get status => _status;
  Regeocode? get regeocode => _regeocode;
  String? get info => _info;
  String? get infocode => _infocode;

  LatLngDecodeBean({String? status, Regeocode? regeocode, String? info, String? infocode}) {
    _status = status;
    _regeocode = regeocode;
    _info = info;
    _infocode = infocode;
  }

  LatLngDecodeBean.fromJson(dynamic json) {
    _status = json['status'] as String;
    _regeocode = json['regeocode'] != null ? Regeocode.fromJson(json['regeocode']) : null;
    _info = json['info'] as String;
    _infocode = json['infocode'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = _status;
    if (_regeocode != null) {
      map['regeocode'] = _regeocode?.toJson();
    }
    map['info'] = _info;
    map['infocode'] = _infocode;
    return map;
  }
}

/// addressComponent : {"city":"杭州市","province":"浙江省","adcode":"330106","district":"西湖区","towncode":"330106109000","streetNumber":{"number":"40号","location":"120.056986,30.319934","direction":"东南","distance":"91.2289","street":"振中路"},"country":"中国","township":"三墩镇","citycode":"0571"}
/// formatted_address : "浙江省杭州市西湖区三墩镇灯彩街"

class Regeocode {
  AddressComponent? _addressComponent;
  String? _formattedAddress;

  AddressComponent? get addressComponent => _addressComponent;
  String? get formattedAddress => _formattedAddress;

  Regeocode({AddressComponent? addressComponent, String? formattedAddress}) {
    _addressComponent = addressComponent;
    _formattedAddress = formattedAddress;
  }

  Regeocode.fromJson(dynamic json) {
    _addressComponent = json['addressComponent'] != null ? AddressComponent.fromJson(json['addressComponent']) : null;
    _formattedAddress = json['formatted_address'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_addressComponent != null) {
      map['addressComponent'] = _addressComponent?.toJson();
    }
    map['formatted_address'] = _formattedAddress;
    return map;
  }
}

/// city : "杭州市"
/// province : "浙江省"
/// adcode : "330106"
/// district : "西湖区"
/// towncode : "330106109000"
/// streetNumber : {"number":"40号","location":"120.056986,30.319934","direction":"东南","distance":"91.2289","street":"振中路"}
/// country : "中国"
/// township : "三墩镇"
/// citycode : "0571"

class AddressComponent {
  String? _city;
  String? _province;
  String? _adcode;
  String? _district;
  String? _towncode;
  StreetNumber? _streetNumber;
  String? _country;
  String? _township;
  String? _citycode;

  String? get city => _city;
  String? get province => _province;
  String? get adcode => _adcode;
  String? get district => _district;
  String? get towncode => _towncode;
  StreetNumber? get streetNumber => _streetNumber;
  String? get country => _country;
  String? get township => _township;
  String? get citycode => _citycode;

  AddressComponent(
      {String? city,
      String? province,
      String? adcode,
      String? district,
      String? towncode,
      StreetNumber? streetNumber,
      String? country,
      String? township,
      String? citycode}) {
    _city = city;
    _province = province;
    _adcode = adcode;
    _district = district;
    _towncode = towncode;
    _streetNumber = streetNumber;
    _country = country;
    _township = township;
    _citycode = citycode;
  }

  AddressComponent.fromJson(dynamic json) {
    _city = json['city'] as String;
    _province = json['province'] as String;
    _adcode = json['adcode'] as String;
    _district = json['district'] as String;
    _towncode = json['towncode'] as String;
    _streetNumber = json['streetNumber'] != null ? StreetNumber.fromJson(json['streetNumber']) : null;
    _country = json['country'] as String;
    _township = json['township'] as String;
    _citycode = json['citycode'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['city'] = _city;
    map['province'] = _province;
    map['adcode'] = _adcode;
    map['district'] = _district;
    map['towncode'] = _towncode;
    if (_streetNumber != null) {
      map['streetNumber'] = _streetNumber?.toJson();
    }
    map['country'] = _country;
    map['township'] = _township;
    map['citycode'] = _citycode;
    return map;
  }
}

/// number : "40号"
/// location : "120.056986,30.319934"
/// direction : "东南"
/// distance : "91.2289"
/// street : "振中路"

class StreetNumber {
  String? _number;
  String? _location;
  String? _direction;
  String? _distance;
  String? _street;

  String? get number => _number;
  String? get location => _location;
  String? get direction => _direction;
  String? get distance => _distance;
  String? get street => _street;

  StreetNumber({String? number, String? location, String? direction, String? distance, String? street}) {
    _number = number;
    _location = location;
    _direction = direction;
    _distance = distance;
    _street = street;
  }

  StreetNumber.fromJson(dynamic json) {
    _number = json['number'] as String;
    _location = json['location'] as String;
    _direction = json['direction'] as String;
    _distance = json['distance'] as String;
    _street = json['street'] as String;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['number'] = _number;
    map['location'] = _location;
    map['direction'] = _direction;
    map['distance'] = _distance;
    map['street'] = _street;
    return map;
  }
}
