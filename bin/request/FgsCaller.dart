import 'dart:convert';

import 'package:dio/dio.dart';

class FgsCaller {
  static final dio = Dio();
  static Future<String> post(String url, Map<String, Object> request) async {
    final data = await dio.post(url, data: request);
    return await json.decode(data.toString());
  }

  static Future<String> get(String url, String query) async {
    final data = await dio.get(url + query);
    return await json.decode(data.toString());
  }
}
