import 'dart:convert';

import 'package:dio/dio.dart';

import '../constant/Constant.dart';
import '../core/log.dart';

final dio = Dio();

class InfiniteFlightLiveDataCaller {
  static Future get(String interfaceType, String data, String interfaceName,
      {int serverType = 0, bool noSession = false}) async {
    try {
      final _sessionValue = noSession ? '' : Links.getSessionId(serverType);
      final _url = 'https://api.infiniteflight.com/public/v2/$interfaceType/$data$interfaceName$_sessionValue';
      var res = await dio.get(_url,
          options: Options(
            responseType: ResponseType.json,
            contentType: 'application/json;charset=UTF-8',
            headers: {'Authorization': 'Bearer vscogyri4dqs9s7rmevka7fc8ypj13zu'},
          ));
      return json.decode(res.toString());
    } catch (e) {
      Log.logger.i(e);
    }
  }

  static Future<Map<String, dynamic>> postLiveData(Map<String, Object> requestData) async {
    final url = 'https://api.infiniteflight.com/public/v2/user/stats/?apikey=vscogyri4dqs9s7rmevka7fc8ypj13zu';
    var res = await dio.post(url, data: requestData);
    final data = json.decode(res.toString());
    return data;
  }
}
