import 'dart:math' as math;

import '../bean/response/AirwayLineListBean.dart';
import '../bean/response/FgsAllFlightsBean.dart';
import '../bean/response/OnlineFlightPlanBean.dart';

class FlightJudgment {
  static String getDepartureTime(List<AirwayLineListBean> res) {
    var departureTime = '';
    for (var i = 0; i < res.length; i++) {
      if (res[i].altitude > res[0].altitude) {
        departureTime = res[i].time;
        break;
      }
    }
    return departureTime;
  }

  static String getFlightHaulType(int range, String departure, String arrival) {
    if (range > 8000) {
      return 'Super Long';
    } else if (range > 5000) {
      return 'Long';
    } else if (range > 1000) {
      return 'Middle Long';
    } else if (range > 500) {
      return 'Middle';
    } else if (range > 100) {
      return 'Short';
    } else if (departure == arrival) {
      return 'Circle training';
    } else {
      return 'Commuter';
    }
  }

  /// 格式化分钟转换为小时H 分钟M
  static String formatFlightTime(int time) {
    var timeFormat = Duration(minutes: time);
    var parts = timeFormat.toString().split(':');
    return '${parts[0]}H ${parts[1]}M';
  }
}

class CalculateRank {
  /// 获取当前角色
  static String getRank(double flightTime) {
    if (flightTime > 50000) {
      return 'Chief Pilot';
    } else if (flightTime > 30000) {
      return 'Instruct Pilot';
    } else if (flightTime > 10000) {
      return 'Captain';
    } else if (flightTime > 5000) {
      return 'Curise Captain';
    } else if (flightTime > 1000) {
      return 'First Officer';
    } else if (flightTime >= 10) {
      return 'Second Officer';
    } else {
      return 'Commander';
    }
  }

  /// 获取下一等级的等级名称以及升级上线
  static RankBean nextRank(String currentRole) {
    final rankList = ['Second Officer', 'First Officer', 'Curise Captain', 'Captain', 'Instruct Pilot', 'Chief Pilot'];
    final _rankReachedValueList = [10.0, 1000.0, 5000.0, 10000.0, 30000.0, 50000.0];
    final _currentIndex = rankList.indexOf(currentRole);
    final _nextRankLimitValue = _rankReachedValueList[_currentIndex + 1];
    return RankBean(nextRole: rankList[_currentIndex + 1], nextRankLimitValue: _nextRankLimitValue);
  }

  /// 根据FGS库内存储的rank转换中文
  static String getFgsRankClassName(int rank) {
    switch (rank) {
      case 0:
        return 'Second Officer';
      case 1:
        return 'First Officer';
      case 2:
        return 'Curise Captain';
      case 3:
        return 'Captain';
      case 4:
        return 'Instruct Pilot';
      case 5:
        return 'Chief Pilot';
      default:
        return 'rookie';
    }
  }
}

class DataHandler {
  static int parseAppVersion(String version) {
    return int.parse(version.replaceAll('.', ''));
  }

  /// 去除字符串中所有空格
  static String trimAllInString(String str) {
    return str.replaceAll(RegExp(r'\s+\b|\b\s'), '');
  }

  /// livery could be empty
  /// we have to give a unknown value when it happened
  static String getLiveryName(List<Livery> livery) {
    if (livery.isEmpty) {
      return 'FGS Airline';
    } else {
      return livery[0].liveryName ?? 'FGS Airline';
    }
  }
}

class RankBean {
  RankBean({required this.nextRole, required this.nextRankLimitValue});
  final String nextRole;
  final double nextRankLimitValue;
}

/// 计算类
class CalculateMethod {
  /// 计算两点之间坐标
  static double getDistance(AirwayLineListBean point1, AirwayLineListBean point2) {
    final radLat1 = point1.latitude * math.pi / 180.0;
    final radLat2 = point2.latitude * math.pi / 180.0;
    final a = radLat1 - radLat2;
    final b = point1.longitude * math.pi / 180.0 - point2.longitude * math.pi / 180.0;
    var s = 2 * math.asin(math.sqrt(math.pow(math.sin(a / 2), 2) + math.cos(radLat1) * math.cos(radLat2) * math.pow(math.sin(b / 2), 2)));
    // EARTH_RADIUS
    s = s * 6371.137;
    s = (s * 10000).round() / 10000;
    return s;
  }

  /// 计算两点之间坐标
  static double getWholeDistance(WholeRangePosition point1, WholeRangePosition point2) {
    final radLat1 = point1.latitude * math.pi / 180.0;
    final radLat2 = point2.latitude * math.pi / 180.0;
    final a = radLat1 - radLat2;
    final b = point1.longitude * math.pi / 180.0 - point2.longitude * math.pi / 180.0;
    var s = 2 * math.asin(math.sqrt(math.pow(math.sin(a / 2), 2) + math.cos(radLat1) * math.cos(radLat2) * math.pow(math.sin(b / 2), 2)));
    // EARTH_RADIUS
    s = s * 6371.137;
    s = (s * 10000).round() / 10000;
    return s;
  }

  /// 计算全航路坐标距离
  static double entireDistance(List<AirwayLineListBean> pointList) {
    var temp = <Map<String, AirwayLineListBean>>[];
    var count = 0.0;
    for (var i = 0; i < pointList.length - 2; i++) {
      temp.add({'point1': pointList[i], 'point2': pointList[i + 2]});
      count = (count + getDistance(temp[i]['point1']!, temp[i]['point2']!));
    }
    return count / 2 / 1.852;
  }

  /// 计算全航路坐标距离
  static double entireWholeDistance(List<WholeRangePosition> pointList) {
    var temp = <Map<String, WholeRangePosition>>[];
    var count = 0.0;
    for (var i = 0; i < pointList.length - 2; i++) {
      temp.add({'point1': pointList[i], 'point2': pointList[i + 2]});
      count = (count + getWholeDistance(temp[i]['point1']!, temp[i]['point2']!));
    }
    return count / 2 / 1.852;
  }
}
