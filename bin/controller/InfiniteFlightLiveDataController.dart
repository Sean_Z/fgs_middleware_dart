import 'dart:convert';

import 'package:shelf/shelf.dart';
import 'package:shelf_router/shelf_router.dart';

import '../api/InfiniteFlightLiveDataApi.dart';
import '../requestbean/FetchSpecificFlightRequestBean.dart';
import '../requestbean/GetAirportInfoRequestBean.dart';
import '../requestbean/GetIFUserByUserNameBean.dart';
import '../requestbean/OnlinePlanRequestBean.dart';

class InfiniteFlightLiveDataController {
  Response sendResult(dynamic data) {
    return Response.ok(json.encode({'data': data}));
  }

  Future<Response> fetchAllFlight(Request request) async {
    final data = await InfiniteFlightLiveDataApi.getAllServerFlight();
    return sendResult({'result': data});
  }

  Future<Response> fetchSpecificFlight(Request request) async {
    final data = await InfiniteFlightLiveDataApi.getAllServerFlight();
    final username = FetchSpecificFlightRequestBean.fromJson(json.decode(await request.readAsString())).username;
    final target = data.where((element) => element.displayName == username).toList();
    if (target.isNotEmpty) {
      return sendResult(target.first.toJson());
    } else {
      return sendResult([]);
    }
  }

  /// 获取在线航班
  Future<Response> fetchOnlinePlan(Request request) async {
    final flightId = OnlinePlanRequestBean.fromJson(json.decode(await request.readAsString())).flightId;
    final data = await InfiniteFlightLiveDataApi.getOnlinePlan(flightId);
    return sendResult(data.toJson());
  }

  /// 获取已飞行航线
  Future<Response> airwayLine(Request request) async {
    final flightId = OnlinePlanRequestBean.fromJson(json.decode(await request.readAsString())).flightId;
    final data = await InfiniteFlightLiveDataApi.getAirwayLine(flightId);
    return sendResult({'data': data});
  }

  /// 获取IF用户信息
  Future<Response> userInfo(Request request) async {
    final username = GetIFUserByUserNameBean.fromJson(json.decode(await request.readAsString())).username;
    final data = await InfiniteFlightLiveDataApi.getInfiniteFlightUserInfoByUserName(username);
    return sendResult(data.toJson());
  }

  /// 获取机场信息
  Future<Response> getAirportInfo(Request request) async {
    final requestBody = GetAirportInfoRequestBean.fromJson(json.decode(await request.readAsString()));
    final data = await InfiniteFlightLiveDataApi.getAirportState(requestBody.airportIcao, requestBody.serverType);
    return sendResult(data.toJson());
  }

  /// 获取FGS成员飞行列表
  Future<Response> getFgsMemberFlight(Request request) async {
    final data = await InfiniteFlightLiveDataApi.getFgsMemberFlight();
    return sendResult(data);
  }

  Future<Response> getFlightCollection(Request request) async {
    final data = await InfiniteFlightLiveDataApi.getFlightCollection();
    return sendResult(data);
  }

  Router get router {
    final router = Router()
      ..post('/fetchFlights', fetchAllFlight)
      ..post('/fetchOnlinePlan', fetchOnlinePlan)
      ..post('/airwayLine', airwayLine)
      ..post('/userInfo', userInfo)
      ..post('/fetchSpecificFlight', fetchSpecificFlight)
      ..post('/getFgsMemberFlight', getFgsMemberFlight)
      ..post('/getAirportInfo', getAirportInfo)
      ..post('/getFlightCollection', getFlightCollection);
    return router;
  }
}
