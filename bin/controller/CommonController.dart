import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../api/InfiniteFlightLiveDataApi.dart';

class CommonController {
  Response sendResult(dynamic data) {
    return Response.ok(json.encode({'data': data}));
  }

  Future<Response> getFlightPlan(Request request) async {
    final data = await InfiniteFlightLiveDataApi.getFlightCollection();
    return sendResult(data);
  }
}
